<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <meta name="description" content="Medcity - Medical Healthcare HTML5 Template">
  <link href="{{asset('assets/images/favicon/favicon.png')}}" rel="icon">
  @yield('title_web')

  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css2?family=Quicksand:wght@400;500;600;700&family=Roboto:wght@400;700&display=swap">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css">
  <link rel="stylesheet" href="{{asset('assets/css/libraries.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
  <style>
    nav {
      background-color: #283b6a;
    }
  </style>
</head>

<body>
  <div class="wrapper">
    <div class="preloader">
      <div class="loading"><span></span><span></span><span></span><span></span></div>
    </div><!-- /.preloader -->

    <!-- =========================
        Header
    =========================== -->
    <header class="header header-layout1">
      <div class="header-topbar">
        <div class="container-fluid">
          <div class="row align-items-center">
            <div class="col-12">
              <div class="d-flex align-items-center justify-content-between">
                <ul class="contact__list d-flex flex-wrap align-items-center list-unstyled mb-0">
                  <li>
                    <button class="miniPopup-emergency-trigger" type="button">Kontak</button>
                    <div id="miniPopup-emergency" class="miniPopup miniPopup-emergency text-center">
                      <div class="emergency__icon">
                        <i class="icon-call3"></i>
                      </div>
                     @yield('kontak-header')
                    </div>
                  </li>
                  <li>
                    <!-- <i class="icon-phone"></i><a href="tel:+5565454117">Emergency Line: (002) 01061245741</a> -->
                  </li>
                  <li>
                    <i class="icon-location"></i><a href="#">Location: Indonesia</a>
                  </li>
                 @yield('current_time')
                </ul><!-- /.contact__list -->
                <div class="d-flex">
                  <ul class="social-icons list-unstyled mb-0 mr-30">
                    @yield('social_media_header')
                  </ul><!-- /.social-icons -->
                  {{-- <form class="header-topbar__search">
                    <input type="text" class="form-control" placeholder="Search...">
                    <button class="header-topbar__search-btn"><i class="fa fa-search"></i></button>
                  </form> --}}
                </div>
              </div>
            </div><!-- /.col-12 -->
          </div><!-- /.row -->
        </div><!-- /.container -->
      </div><!-- /.header-top -->
      <nav class="navbar navbar-expand-lg sticky-navbar" >
        <div class="container-fluid" >
          <a class="navbar-brand" href="https://kampoonghening.com/">
            @yield('logo_header')
          </a>
          <button class="navbar-toggler" type="button">
            <span class="menu-lines"><span></span></span>
          </button>
          <div class="collapse navbar-collapse" id="mainNavigation">
            <ul class="navbar-nav ml-auto">
              <li class="nav__item has-dropdown">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle nav__item-link" >Layanan</a>
                <ul class="dropdown-menu">
                  <li class="nav__item">
                    <a href="https://konten.kampoonghening.com/klinik-wisata/" class="nav__item-link">Eco Therapy</a>
                  </li><!-- /.nav-item -->
                  <li class="nav__item">
                    <a href="https://konten.kampoonghening.com/mindful-professional/" class="nav__item-link">Silent Mindful Listening</a>
                  </li><!-- /.nav-item -->
                  <li class="nav__item">
                    <a href="https://konten.kampoonghening.com/tools-healing-development/" class="nav__item-link">Konseling dan Tools Healing</a>
                  </li><!-- /.nav-item -->
                  <li class="nav__item">
                    <a href="https://konten.kampoonghening.com/be-a-counselor-blended-learning/" class="nav__item-link">Be a Happy Counselor</a>
                  </li><!-- /.nav-item -->
                </ul><!-- /.dropdown-menu -->
              </li><!-- /.nav-item -->
              <li class="nav__item has-dropdown">
                <a href="https://konten.kampoonghening.com/kerjasama-dengan-kampoong-hening/" class=" nav__item-link" >Kerjasama</a>
              </li><!-- /.nav-item -->
              <li class="nav__item has-dropdown">
                <a href="https://shopee.co.id/welcometokampoonghening?v=0fa&smtt=0.0.3" class="nav__item-link" >Belanja</a>
              </li><!-- /.nav-item -->
              <li class="nav__item has-dropdown">
                @yield('ebook')
                <!-- <ul class="dropdown-menu">
                  <li class="nav__item">
                    <a href="blog.html" class="nav__item-link">Blog Grid</a>
                  </li>
                  <li class="nav__item">
                    <a href="blog-single-post.html" class="nav__item-link">Single Blog Post</a>
                  </li>
                </ul> -->
              </li>
              {{-- <li class="nav__item has-dropdown">
                <a href="https://program.kampoonghening.id/the-new-me-online/" class="nav__item-link" >Afiliasi</a>
              </li><!-- /.nav-item --> --}}
            </ul><!-- /.navbar-nav -->
            <button class="close-mobile-menu d-block d-lg-none"><i class="fas fa-times"></i></button>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container -->
      </nav><!-- /.navabr -->
    </header><!-- /.Header -->

    @yield('content')

    <!-- ========================
      Footer
    ========================== -->
    @yield('footer')
    <button id="scrollTopBtn"><i class="fas fa-long-arrow-alt-up"></i></button>
  </div><!-- /.wrapper -->

  <script src="{{asset('assets/js/jquery-3.5.1.min.js')}}"></script>
  <script src="{{asset('assets/js/plugins.js')}}"></script>
  <script src="{{asset('assets/js/main.js')}}"></script>
</body>

</html>