@extends('layouts.dashboard')
@section('dashboard-favicon')
    <link rel="icon" href="{{asset('assets/images/favicon/'.$web_setting->favicon.'')}}" type="image/x-icon">
@endsection
@section('bearcrumb')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5 class="m-b-10">Edit Data</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#!">Post Gallery</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="alert alert-info" role="alert" id="success">
    Data Berhasil Disimpan
</div>
<div class="row">
    <!-- prject ,team member start -->
    <div class="col-xl-12 col-md-12">
        <div class="card table-card">
            <div class="card-header">
                <h3>Edit Data</h3>
            </div>
            <div class="card-body p-0">
                <div class="container-fluid">
                    @if ($section == 'slider')
                    <form class="form-group" method="POST" action="{{route('edit-save')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="row mb-3">
                            <div class="form-group col-xl-3 col-md-6  mt-3">
                                <label for="exampleFormControlTextarea1"><b> Title Slider</b></label>
                                <input type="text" class="form-control" id="title" name="section" aria-describedby="title" placeholder="Masukan Title" value="{{$section}}" style="display: none;">
                                <input type="text" class="form-control" id="title" name="id" aria-describedby="title" placeholder="Masukan Title" value="{{$id}}" style="display: none;">
                                <input type="text" class="form-control" id="title" name="title" aria-describedby="title" placeholder="Masukan Title" value="{{$data->title}}">
                                <small id="title" class="form-text text-muted">Ubah title sesuai kebutuhan</small>
                                {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                            </div>
                            <div class="form-group col-xl-3 col-md-6  mt-3">
                                <label for="exampleFormControlTextarea1"><b> Caption Slider</b></label>
                                <input type="text" class="form-control" id="caption" name="caption" aria-describedby="title" placeholder="Masukan Caption" value="{{$data->caption}}">
                                <small id="caption" class="form-text text-muted">Ubah caption sesuai kebutuhan</small>
                                {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                            </div>
                            <div class="col-md-6 col-xl-3 mt-3">
                                <label for="exampleFormControlTextarea1"><b> Image Slider</b></label>
                                <div class="input-group">
                                    <div class="input-group">
                                        <div class="form-group">
                                            <input type="file" class="form-control-file" id="img" name="img">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-3 mt-4">
                                <div class="input-group">
                                    <div class="input-group">
                                        <div class="form-group">
                                            <button type="submit" class="btn  btn-primary mb-2">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    @endif

                    @if ($section == 'founder')
                    <form class="form-group" method="POST" action="{{route('edit-save')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="row mb-3">
                            {{-- <div class="form-group col-xl-3 col-md-6  mt-3">
                                <label for="exampleFormControlTextarea1"><b>Nama</b></label>
                                
                                <input type="text" class="form-control" id="title" name="id" aria-describedby="title" placeholder="Masukan Title" value="{{$id}}" style="display: none;">
                                <input type="text" class="form-control" id="title" name="title" aria-describedby="title" placeholder="Masukan Title" value="{{$data->title}}">
                                <small id="title" class="form-text text-muted">Ubah title sesuai kebutuhan</small>
                            </div> --}}
                            <div class="form-group col-xl-3 col-md-6  mt-3">
                                <input type="text" class="form-control" id="title" name="id" aria-describedby="title" placeholder="Masukan Title" value="{{$id}}" style="display: none;">
                                <input type="text" class="form-control" id="title" name="section" aria-describedby="title" placeholder="Masukan Title" value="{{$section}}" style="display: none;">
                                <label for="exampleFormControlTextarea1"><b>Nama</b></label>
                                <input type="text" class="form-control" id="nama" name="nama" aria-describedby="title" placeholder="Masukan Nama" value="{{$data->nama}}">
                                <small id="title" class="form-text text-muted">Ubah nama sesuai kebutuhan</small>
                            </div>
                            <div class="form-group col-xl-3 col-md-6  mt-3">
                                <label for="exampleFormControlTextarea1"><b>Jabatan</b></label>
                                <input type="text" class="form-control" id="jabatan" name="jabatan" aria-describedby="title" placeholder="Masukan jabatan" value="{{$data->jabatan}}">
                                <small id="caption" class="form-text text-muted">Ubah jabatan sesuai kebutuhan</small>
                            </div>
                            <div class="form-group col-xl-3 col-md-6  mt-3">
                                <label for="exampleFormControlTextarea1"><b>Caption</b></label>
                                <input type="text" class="form-control" id="caption" name="caption" aria-describedby="title" placeholder="Masukan Caption" value="{{$data->caption}}">
                                <small id="caption" class="form-text text-muted">Ubah caption sesuai kebutuhan</small>
                            </div>
                            <div class="col-md-6 col-xl-3 mt-3">
                                <label for="exampleFormControlTextarea1"><b>Foto</b></label>
                                <div class="input-group">
                                    <div class="input-group">
                                        <div class="form-group">
                                            <input type="file" class="form-control-file" id="foto" name="img">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-xl-3 col-md-6  mt-3">
                                <label for="exampleFormControlTextarea1"><b>Link Facebook</b></label>
                                <input type="text" class="form-control" id="link_facebook" name="link_facebook" aria-describedby="link_facebook" placeholder="Masukan link_facebook" value="{{$data->link_facebook}}">
                                <small id="link_facebook" class="form-text text-muted">Ubah link facebook sesuai kebutuhan</small>
                            </div>
                            <div class="form-group col-xl-3 col-md-6  mt-3">
                                <label for="exampleFormControlTextarea1"><b>Link Instagram</b></label>
                                <input type="text" class="form-control" id="link_instagram" name="link_instagram" aria-describedby="title" placeholder="Masukan link_instagram" value="{{$data->link_instagram}}">
                                <small id="link_instagram" class="form-text text-muted">Ubah link instagram sesuai kebutuhan</small>
                            </div>
                            <div class="form-group col-xl-3 col-md-6  mt-3">
                                <label for="exampleFormControlTextarea1"><b>Link Twitter</b></label>
                                <input type="text" class="form-control" id="link_twitter" name="link_twitter" aria-describedby="title" placeholder="Masukan link_twitter" value="{{$data->link_twitter}}">
                                <small id="link_twitter" class="form-text text-muted">Ubah link twitter sesuai kebutuhan</small>
                            </div>
                            <div class="col-md-6 col-xl-3 mt-4">
                                <div class="input-group">
                                    <div class="input-group">
                                        <div class="form-group">
                                            <button type="submit" class="btn  btn-primary mb-2">Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    @endif

                    @if ($section == 'gallery')
                    <form class="form-group" method="POST" action="{{route('edit-save')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="row mb-3">
                            {{-- <div class="form-group col-xl-3 col-md-6  mt-3">
                                <label for="exampleFormControlTextarea1"><b>Nama</b></label>
                                
                                <input type="text" class="form-control" id="title" name="id" aria-describedby="title" placeholder="Masukan Title" value="{{$id}}" style="display: none;">
                                <input type="text" class="form-control" id="title" name="title" aria-describedby="title" placeholder="Masukan Title" value="{{$data->title}}">
                                <small id="title" class="form-text text-muted">Ubah title sesuai kebutuhan</small>
                            </div> --}}
                            <div class="col-md-6 col-xl-6 mt-3">
                                <input type="text" class="form-control" id="title" name="id" aria-describedby="title" placeholder="Masukan Title" value="{{$id}}" style="display: none;">
                                <input type="text" class="form-control" id="title" name="section" aria-describedby="title" placeholder="Masukan Title" value="{{$section}}" style="display: none;">
                                <label for="exampleFormControlTextarea1"><b>Foto</b></label>
                                <div class="input-group">
                                    <div class="input-group">
                                        <div class="form-group">
                                            <input type="file" class="form-control-file" id="img" name="img">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-6 mt-4">
                                <div class="input-group">
                                    <div class="input-group">
                                        <div class="form-group">
                                            <button type="submit" class="btn  btn-primary mb-2">Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    @endif

                    @if ($section == 'testimoni')
                    <form class="form-group" method="POST" action="{{route('edit-save')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="row mb-3">
                            {{-- <div class="form-group col-xl-3 col-md-6  mt-3">
                                <label for="exampleFormControlTextarea1"><b>Nama</b></label>
                                
                                <input type="text" class="form-control" id="title" name="id" aria-describedby="title" placeholder="Masukan Title" value="{{$id}}" style="display: none;">
                                <input type="text" class="form-control" id="title" name="title" aria-describedby="title" placeholder="Masukan Title" value="{{$data->title}}">
                                <small id="title" class="form-text text-muted">Ubah title sesuai kebutuhan</small>
                            </div> --}}
                            <div class="form-group col-xl-3 col-md-6  mt-3">
                                <input type="text" class="form-control" id="title" name="id" aria-describedby="title" placeholder="Masukan Title" value="{{$id}}" style="display: none;">
                                <input type="text" class="form-control" id="title" name="section" aria-describedby="title" placeholder="Masukan Title" value="{{$section}}" style="display: none;">
                                <label for="exampleFormControlTextarea1"><b>Nama</b></label>
                                <input type="text" class="form-control" id="nama" name="nama" aria-describedby="title" placeholder="Masukan Nama" value="{{$data->nama}}">
                                <small id="title" class="form-text text-muted">Ubah nama sesuai kebutuhan</small>
                            </div>
                            <div class="form-group col-xl-3 col-md-6  mt-3">
                                <label for="exampleFormControlTextarea1"><b>Jabatan</b></label>
                                <input type="text" class="form-control" id="jabatan" name="jabatan" aria-describedby="title" placeholder="Masukan jabatan" value="{{$data->jabatan}}">
                                <small id="caption" class="form-text text-muted">Ubah jabatan sesuai kebutuhan</small>
                            </div>
                            <div class="col-md-3 col-xl-6 mt-3">
                                <label for="exampleFormControlTextarea1"><b>Foto</b></label>
                                <div class="input-group">
                                    <div class="input-group">
                                        <div class="form-group">
                                            <input type="file" class="form-control-file" id="img" name="img">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-xl-3 col-md-6  mt-3">
                                <label for="exampleFormControlTextarea1"><b>Caption</b></label>
                                {{-- <input type="text" class="form-control" id="caption" name="caption" aria-describedby="title" placeholder="Masukan Caption"> --}}
                                <textarea class="form-control" id="exampleFormControlTextarea1" name="caption" rows="4">{{$data->caption}}</textarea>
                                <small id="caption" class="form-text text-muted">Ubah caption sesuai kebutuhan</small>
                            </div>
                           
                            <div class="col-md-6 col-xl-3 mt-5">
                                <div class="input-group">
                                    <div class="input-group">
                                        <div class="form-group">
                                            <button type="submit" class="btn  btn-primary mb-2">Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('custom-script')
    @if($errors->any())
        <script>
            window.addEventListener('load', function() {
            $("#success").show().delay(5000).fadeOut();
        })
        </script>
    @else
        <script>
            window.addEventListener('load', function() {
                $("#success").hide();
            })
        </script>
    @endif
    <script>
        // function onChange(file) {
        //     console.log(file)
        //     const [file] = file
        //     if (file) {
        //     blah.src = URL.createObjectURL(file)
        //     }
        // }
        favicon.onchange = function(event) {
            console.log('test')
        var previewfavicon = document.getElementById("preview-favicon");
        var fileList = favicon.files;
            if (fileList) {
                previewfavicon.src = URL.createObjectURL(fileList[0])
            }  
        }

        logo.onchange = function(event) {
        var previewlogo = document.getElementById("preview-logo");
        var fileList = logo.files;
            if (fileList) {
                previewlogo.src = URL.createObjectURL(fileList[0])
            }  
        }

        // slider_2.onchange = function(event) {
        // var previewSlider = document.getElementById("preview-slider-2");
        // var fileList = slider_2.files;
        // console.log(fileList)
        //     if (fileList) {
        //         previewSlider.src = URL.createObjectURL(fileList[0])
        //     }  
        // }

        // slider_3.onchange = function(event) {
        // var previewSlider = document.getElementById("preview-slider-3");
        // var fileList = slider_3.files;
        // console.log(fileList)
        //     if (fileList) {
        //         previewSlider.src = URL.createObjectURL(fileList[0])
        //     }  
        // }
    </script>
@endsection