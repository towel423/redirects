@extends('layouts.landingpage')
@section('title_web')
	<title>{{ $web_setting->title }}</title>
@endsection
@section('kontak-header')
<a href="https://api.whatsapp.com/send?phone={{ $header_setting->no_telp }}" class="phone__number">
	<i class="icon-phone"></i> <span>+{{ $header_setting->no_telp }}</span>
  </a>
  <p>{{ $header_setting->caption }}</p>
  <a href="https://api.whatsapp.com/send?phone={{ $header_setting->no_telp }}" class="btn btn__secondary btn__link btn__block">
	<span>Konsultasi</span> <i class="icon-arrow-right"></i>
  </a>
@endsection
@section('current_time')
	<li>
	<i class="icon-clock"></i><a href="#">{{ $current_time }}</a>
  </li>
@endsection
@section('social_media_header')
	<li><a href="{{$header_setting->link_facebook}}" style="background-color: white;"><i class="fab fa-facebook-f"></i></a></li>
	<li><a href="{{$header_setting->link_instagram}}" style="background-color: white;"><i class="fab fa-instagram"></i></a></li>
	<li><a href="{{$header_setting->link_twitter}}" style="background-color: white;"><i class="fab fa-twitter"></i></a></li>
@endsection
@section('logo_header')
<img src="{{asset('assets/images/logo/'.$web_setting->logo.'')}}"  alt="logo" style="height: 70px;">
@endsection
@section('footer')
<footer class="footer">
	<div class="footer-primary">
	  <div class="container">
		<div class="row">
		  <div class="col-sm-12 col-md-12 col-lg-3">
			<div class="footer-widget-about">
			  <img src="{{asset('assets/images/logo/'.$web_setting->logo_footer.'')}}" alt="logo" class="mb-30">
			  <p class="color-gray">{{$header_setting->caption}}
			  </p>
			  <a href="https://api.whatsapp.com/send?phone={{ $header_setting->no_telp }}" class="btn btn__primary btn__primary-style2 btn__link">
				<span>Konsultasi</span> <i class="icon-arrow-right"></i>
			  </a>
			</div><!-- /.footer-widget__content -->
		  </div><!-- /.col-xl-2 -->
		  <div class="col-sm-6 col-md-6 col-lg-2 offset-lg-1">
			<div class="footer-widget-nav">
			  <h6 class="footer-widget__title">Social Media</h6>
				<ul class="list-unstyled">
				  <li><a href="{{$header_setting->link_facebook}}"><i class="fab fa-facebook-f"></i> Facebook</a></li>
				  <li><a href="{{$header_setting->link_instagram}}"><i class="fab fa-instagram"></i> Instagram</a></li>
				  <li><a href="{{$header_setting->link_twitter}}"><i class="fab fa-twitter"></i> Twitter</a></li>
				</ul>
			</div><!-- /.footer-widget__content -->
		  </div><!-- /.col-lg-2 -->
		  <div class="col-sm-6 col-md-6 col-lg-2">
			<div class="footer-widget-nav">
			  <h6 class="footer-widget__title">Links</h6>
				<ul class="list-unstyled">
				  <li><a href="#">Home</a></li>
				  <li><a href="https://konten.kampoonghening.com/mindful-professional/">Silent Mindful Listening</a></li>
				  <li><a href="https://konten.kampoonghening.com/klinik-wisata/">Eco Therapy</a></li>
				  <li><a href="https://konten.kampoonghening.com/tools-healing-development/">Konseling dan Tools Healing</a></li>
				  <li><a href="https://konten.kampoonghening.com/be-a-counselor-blended-learning/">Be a Happy Counselor</a></li>
				  {{-- <li><a href="https://program.kampoonghening.id/the-new-me-online/">Afiliasi</a></li> --}}
				</ul>
			</div><!-- /.footer-widget__content -->
			{{-- Header Ebook --}}
			@section('ebook')
				<a href="{{ $content_section_11->no_telp }}" class=" nav__item-link" >E-Book Gratis</a>
			@endsection
		  </div><!-- /.col-lg-2 -->
		  <div class="col-sm-12 col-md-6 col-lg-4">
			<div class="footer-widget-contact">
			  <h6 class="footer-widget__title color-heading">Kontak Kami</h6>
			  <ul class="contact-list list-unstyled">
				<li>Jika ada pertanyaan silahkan kontak nomor yang tertera, tim kami siap membantu.</li>
				<li>
				  <a href="https://api.whatsapp.com/send?phone={{ $header_setting->no_telp }}" class="phone__number">
					<i class="icon-phone"></i> <span>+{{ $header_setting->no_telp }}</span>
				  </a>
				</li>
				<li class="color-body">{{ $web_setting->alamat }}.</li>
			  </ul>
			  <div class="d-flex align-items-center">
				<!-- <a href="contact-us.html" class="btn btn__primary btn__link mr-30">
				  <i class="icon-arrow-right"></i> <span>Get Directions</span>
				</a> -->
				<!-- <ul class="social-icons list-unstyled mb-0">
				  <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
				  <li><a href="#"><i class="fab fa-instagram"></i></a></li>
				  <li><a href="#"><i class="fab fa-twitter"></i></a></li>
				</ul>/.social-icons -->
			  </div>
			</div><!-- /.footer-widget__content -->
		  </div><!-- /.col-lg-2 -->
		</div><!-- /.row -->
	  </div><!-- /.container -->
	</div><!-- /.footer-primary -->
	<div class="footer-secondary">
	  <div class="container">
		<div class="row align-items-center">
		  <div class="col-sm-12 col-md-6 col-lg-6">
			<span class="fz-14">&copy; 2021 Kampoong Hening, All Rights Reserved.</span>
		  
		  </div><!-- /.col-lg-6 -->
		  <div class="col-sm-12 col-md-6 col-lg-6">
			<nav>
			  <ul class="list-unstyled footer__copyright-links d-flex flex-wrap justify-content-end mb-0" style="background-color: white;">
				<li><a href="#">Terms & Conditions</a></li>
				<li><a href="#">Privacy Policy</a></li>
				<li><a href="#">Cookies</a></li>
			  </ul>
			</nav>
		  </div><!-- /.col-lg-6 -->
		</div><!-- /.row -->
	  </div><!-- /.container -->
	</div><!-- /.footer-secondary -->
  </footer><!-- /.Footer -->
@endsection
@section('content')
	<!-- ============================
        Slider
    ============================== -->
    <section class="slider">
		<div class="slick-carousel m-slides-0"
		  data-slick='{"slidesToShow": 1,  "autoplay": true, "arrows": true, "dots": false, "speed": 4000, "infinite": true, "fade": true,"cssEase": "linear"}'>
		  @foreach ($slider as $item)
		  <div class="slide-item align-v-h">
			<div class="bg-img"><img src="{{asset('assets/images/sliders/'.$item->img.'')}}" alt="slide img"></div>
			<div class="container">
			  <div class="row align-items-center">
				<div class="col-sm-12 col-md-12 col-lg-12 col-xl-7">
				  <div class="slide__content">
					<h2 class="slide__title">{{ $item->title }}</h2>
					<p class="slide__desc">{{ $item->caption }}</p>
					<ul class="features-list list-unstyled mb-0 d-flex flex-wrap">
					  <!-- feature item #1 -->
					  <li class="feature-item">
						<div class="feature__icon">
						  <a href="{{route('compro')}}" target="_blank" style="color: #213360"><i class="icon-search"></i></a>
						</div>
						<div>
							<h2 class="feature__title">Temukan</h2>	
							<h2 class="feature__title">Solusi</h2>
						</div>	
					  </li><!-- /.feature-item-->
					  <!-- feature item #2 -->
					  <li class="feature-item">
						<div class="feature__icon">
						  <a href="#" style="color: #213360"><i class="icon-user"></i></a>
						</div>
						<h2 class="feature__title">Pelajari</h2>
						<h2 class="feature__title">Dulu</h2>
					  </li><!-- /.feature-item-->
					</ul><!-- /.features-list -->
				  </div><!-- /.slide-content -->
				</div><!-- /.col-xl-7 -->
			  </div><!-- /.row -->
			</div><!-- /.container -->
		  </div><!-- /.slide-item -->
		  @endforeach
		</div><!-- /.carousel -->
	  </section><!-- /.slider -->
  
	  <!-- ============================
		  contact info
	  ============================== -->
	  <section class="contact-info py-0">
		<div class="container">
		  <div class="row row-no-gutter boxes-wrapper">
			  @foreach ($kontak_info as $item)
			  <div class="col-sm-12 col-md-4">
				<div class="contact-box d-flex">
				  <div class="contact__icon">
					<i class="icon-call3"></i>
				  </div><!-- /.contact__icon -->
				  @if($item->title == 'Konsultasi Online')
				  <div class="contact__content">
					<h2 class="contact__title">{{ $item->title }}</h2>
					<p class="contact__desc">{{ $item->caption }}</p>
					<a href="https://api.whatsapp.com/send?phone=62857-1757-0150" class="phone__number">
					  <i class="icon-phone"></i> <span>+62857-1757-0150</span>
					</a>
				  </div><!-- /.contact__content -->
				  @else
				  <div class="contact__content">
					<h2 class="contact__title">{{ $item->title }}</h2>
					<p class="contact__desc">{{ $item->caption }}</p>
					<a href="https://api.whatsapp.com/send?phone={{ $header_setting->no_telp }}" class="phone__number">
					  <i class="icon-phone"></i> <span>+{{$header_setting->no_telp}}</span>
					</a>
				  </div><!-- /.contact__content -->
				  @endif
				</div><!-- /.contact-box -->
			  </div><!-- /.col-md-4 -->
			  @endforeach
			{{-- <div class="col-sm-12 col-md-4">
			  <div class="contact-box d-flex">
				<div class="contact__icon">
				  <i class="icon-health-report"></i>
				</div><!-- /.contact__icon -->
				<div class="contact__content">
				  <h2 class="contact__title">Jadwal Konsultasi</h2>
				  <p class="contact__desc">Qualified doctors available six days a week, view our timetable to make an
					appointment.</p>
					<a href="https://api.whatsapp.com/send?phone={{ $header_setting->no_telp }}" class="phone__number">
						<i class="icon-phone"></i> <span>+{{$header_setting->no_telp}}</span>
					  </a>
				</div><!-- /.contact__content -->
			  </div><!-- /.contact-box -->
			</div><!-- /.col-md-4 --> --}}
			<div class="col-sm-12 col-md-4">
			  <div class="contact-box d-flex">
				<div class="contact__icon">
				  <i class="icon-heart2"></i>
				</div><!-- /.contact__icon -->
				<div class="contact__content">
				  <h2 class="contact__title">Jam Buka</h2>
				  <ul class="time__list list-unstyled mb-0">
					@foreach ($jam_buka as $item)
					<li><span class="mr-2">{{ $item->hari }}</span><span> {{$item->jam}}</span></li>
					@endforeach
					
				  </ul>
				</div><!-- /.contact__content -->
			  </div><!-- /.contact-box -->
			</div><!-- /.col-md-4 -->
		  </div><!-- /.row -->
		</div><!-- /.container -->
	  </section><!-- /.contact-info -->
  
	  <!-- ========================
		About Layout 2
	  =========================== -->
	  <section class="about-layout2 pb-0">
		<div class="container">
		  <div class="row">
			<div class="col-sm-12 col-md-12 col-lg-7 offset-lg-1">
			  <div class="heading-layout2">
				<h3 class="heading__title mb-60">{!!$content_section_2->title!!}</h3>
			  </div><!-- /heading -->
			</div><!-- /.col-12 -->
		  </div><!-- /.row -->
		  <div class="row">
			<div class="col-sm-12 col-md-12 col-lg-5">
			  <div class="text-with-icon">
				<div class="text__icon">
				  <i class="icon-doctor"></i>
				</div>
				<div class="text__content">
				  <p class="heading__desc font-weight-bold color-secondary mb-30" style="text-align: justify;">{!!$content_section_2->caption!!}
				  </p>
				</div>
			  </div>
			  <div class="d-flex flex-row bd-highlight mb-20">
				<div class="p-2 bd-highlight">
					<a href="https://api.whatsapp.com/send?phone={{ $header_setting->no_telp }}" class="btn btn__secondary btn__rounded">
						<span>Konsultasikan</span> <i class="icon-arrow-right"></i>
					</a>
				</div>
			  </div>
			  <div class="post-item">
				<div class="post__img">
				<iframe style="width: 100%; height: 300px;" src="https://www.youtube.com/embed/3Q3mQNodgyg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div><!-- /.post__img -->
				<div class="post__body">
				</div><!-- /.post__body -->
			  </div><!-- /.post-item -->
			  {{-- <div class="video-banner-layout2 bg-overlay">
				<img src="{{asset('assets/images/about/2.jpg')}}" alt="about" class="w-100">
				<a class="video__btn video__btn-white popup-video" href="https://www.youtube.com/watch?v=3Q3mQNodgyg">
				  <div class="video__player">
					<i class="fa fa-play"></i>
				  </div>
				  <span class="video__btn-title color-white">Video Kami</span>
				</a>
			  </div><!-- /.video-banner --> --}}
			</div><!-- /.col-lg-6 -->
			<div class="col-sm-12 col-md-12 col-lg-7">
			  <div class="about__text bg-white">
				<p class="heading__desc mb-30" style="text-align: justify;">{!!$content_section_2->caption1!!}</p>
				<ul class="list-items list-unstyled">
				  <li>{!!$content_section_2->caption2!!}</li>
				  <li>{!!$content_section_2->caption3!!}
				  </li>
				  <li>{!!$content_section_2->caption4!!}
				  </li>
				</ul>
			  </div>
			</div><!-- /.col-lg-6 -->
		  </div><!-- /.row -->
		</div><!-- /.container -->
	  </section><!-- /.About Layout 2 -->
  
	  <!-- ========================
		  Services Layout 1
	  =========================== -->
	  <section class="services-layout1 services-carousel">
		<div class="bg-img"><img src="{{asset('assets/images/backgrounds/'.$web_setting->banner_section_3.'')}}" alt="background"></div>
		<div class="container">
		  <div class="row">
			<div class="col-sm-12 col-md-12 col-lg-6 offset-lg-3">
			  <div class="heading text-center mb-60">
				<h2 class="heading__subtitle"></h2>
				<h3 class="heading__title" style="color: white;">{{$content_section_3->title}}</h3>
			  </div><!-- /.heading -->
			</div><!-- /.col-lg-6 -->
		  </div><!-- /.row -->
		  <div class="row">
			<div class="col-12">
			  <div class="slick-carousel"
				data-slick='{"slidesToShow": 3, "slidesToScroll": 1, "autoplay": true, "arrows": false, "dots": true, "responsive": [ {"breakpoint": 992, "settings": {"slidesToShow": 2}}, {"breakpoint": 767, "settings": {"slidesToShow": 1}}, {"breakpoint": 480, "settings": {"slidesToShow": 1}}]}'>
				@php
					use App\Models\CaptionDetail;
					$caption_detail = CaptionDetail::where('id_landing_page',$content_section_3->id)->get();
				@endphp
				@foreach ($caption_detail as $item)
					<!-- service item #1 -->
				<div class="service-item">
					<div class="service__icon">
					  <i class="{{$item->icon}}"></i>
					  <i class="{{$item->icon}}"></i>
					</div><!-- /.service__icon -->
					<div class="service__content">
					  <h4 class="service__title">{{$item->title_detail}}</h4>
					  <p class="service__desc">
					  </p>
					  <ul class="list-items list-items-layout1 list-unstyled">
						  @if($item->id == 4)
						 	 <li class="mb-100">{{$item->caption2}}</li>
						  @else
						  	<li>{{$item->caption2}}</li>
							<li>{{$item->caption3}}</li>
							<li>{{$item->caption4}}</li>
						  @endif
					  </ul>
					  <a href="https://api.whatsapp.com/send?phone={{ $header_setting->no_telp }}" class="btn btn__secondary btn__outlined btn__rounded">
						<span>Konsultasikan</span>
						<i class="icon-arrow-right"></i>
					  </a>
					</div><!-- /.service__content -->
				  </div><!-- /.service-item -->
  
				@endforeach
				
			  </div>
			</div><!-- /.col-12 -->
		  </div><!-- /.row -->
		</div><!-- /.container -->
	  </section><!-- /.Services Layout 1 -->
  
	  <!-- ========================
		  Notses
	  =========================== -->
	  {{-- <section class="notes border-top pt-60 pb-60">
		<div class="container">
		  <div class="row">
			<div class="col-sm-12 col-md-12 col-lg-6">
			  <div class="note font-weight-bold">
				<i class="far fa-file-alt color-primary"></i>
				<span>Delivering tomorrow’s health care for your family.</span>
				<a href="doctors-timetable.html" class="btn btn__link btn__secondary">
				  <span>View Doctors’ Timetable</span><i class="icon-arrow-right"></i>
				</a>
			  </div>
			</div><!-- /.col-sm-6 -->
			<div class="col-sm-12 col-md-12 col-lg-6">
			  <div class="info__meta d-flex flex-wrap justify-content-between align-items-center">
				<div class="testimonials__rating">
				  <div class="testimonials__rating-inner d-flex align-items-center">
					<span class="total__rate">4.9</span>
					<div>
					  <span class="overall__rate">Zocdoc Overall Rating</span>
					  <span>, based on 7541 reviews.</span>
					</div>
				  </div><!-- /.testimonials__rating-inner -->
				</div><!-- /.testimonials__rating -->
				<a href="appointment.html" class="btn btn__primary btn__rounded">
				  <span>Make Appointment</span> <i class="icon-arrow-right"></i>
				</a>
			  </div><!-- /.info__meta -->
			</div><!-- /.col-sm-6 -->
		  </div><!-- /.row -->
		</div><!-- /.container -->
	  </section><!-- /.notes --> --}}
  
	  <!-- ======================
	  Features Layout 2
	  ========================= -->
	  <section class="features-layout2 pt-130 bg-overlay bg-overlay-primary">
		<div class="bg-img"><img src="{{asset('assets/images/backgrounds/2.jpg')}}" alt="background"></div>
		<div class="container">
		  <div class="row">
			<div class="col-sm-12 col-md-12 col-lg-8 offset-lg-1">
			  <div class="heading__layout2 mb-50">
				<h3 class="heading__title color-white" style="text-align: justify;">{{$content_section_4->title}}</h3>
			  </div>
			</div><!-- /col-lg-5 -->
		  </div><!-- /.row -->
		  <div class="row mb-100">
			<div class="col-sm-3 col-md-3 col-lg-1 offset-lg-5">
			  <div class="heading__icon">
				<i class="icon-insurance"></i>
			  </div>
			</div><!-- /.col-lg-5 -->
			<div class="col-sm-9 col-md-9 col-lg-6">
			  <p class="heading__desc font-weight-bold color-white mb-30" style="text-align: justify;">{{$content_section_4->caption}}
			  </p>
			  {{-- <a href="#" class="btn btn__white btn__link">
				<i class="icon-arrow-right icon-filled"></i>
				<span>Our Core Values</span>
			  </a> --}}
			</div><!-- /.col-lg-6 -->
		  </div><!-- /.row -->
		  <div class="row">
			@php
				$detail = CaptionDetail::where('id_landing_page',$content_section_4->id)->get();
			@endphp
			@foreach ($detail as $item)
			<!-- Feature item #1 -->
			<div class="col-sm-6 col-md-6 col-lg-4">
				<div class="feature-item">
				<div class="feature__img">
					<img src="{{asset('assets/images/services/'.$item->img.'')}}" alt="service" loading="lazy">
				</div><!-- /.feature__img -->
				<div class="feature__content">
					<div class="feature__icon">
					<i class="{{$item->icon}}"></i>
					</div>
					<h4 class="feature__title">{{$item->title_detail}}</h4>
				</div><!-- /.feature__content -->
				<a href="#" class="btn__link">
					<i class="icon-arrow-right icon-outlined"></i>
				</a>
				</div><!-- /.feature-item -->
			</div><!-- /.col-lg-3 -->
			@endforeach
			<!-- Feature item #8 -->
			</div><!-- /.col-lg-3 -->
		  </div><!-- /.row -->
		  <div class="row">
			<div class="col-md-12 col-lg-6 offset-lg-3 text-center">
			  <p class="font-weight-bold color-gray mb-0">
				<a href="https://api.whatsapp.com/send?phone={{ $header_setting->no_telp }}" class="color-secondary">
				  <span>Kontak kami untuk informasi lebih lanjut</span> <i class="icon-arrow-right"></i>
				</a>
			  </p>
			</div><!-- /.col-lg-6 -->
		  </div><!-- /.row -->
		</div><!-- /.container -->
	  </section><!-- /.Features Layout 2 -->
  
	  <!-- ======================
		Team
	  ========================= -->
	  <section class="team-layout2 pt-5 pb-80">
		<div class="container">
		  <div class="row">
			<div class="col-sm-12 col-md-12 col-lg-6 offset-lg-3">
			  <div class="heading text-center mb-40">
				<h3 class="heading__title"> Meet Our Founder</h3>
				<p class="heading__desc">
				</p>
			  </div><!-- /.heading -->
			</div><!-- /.col-lg-6 -->
		  </div><!-- /.row -->
		  <div class="row">
			<div class="col-12">
			  <div class="slick-carousel"
				data-slick='{"slidesToShow": 3, "slidesToScroll": 1, "autoplay": true, "arrows": true, "dots": false, "responsive": [ {"breakpoint": 992, "settings": {"slidesToShow": 2}}, {"breakpoint": 767, "settings": {"slidesToShow": 1}}, {"breakpoint": 480, "settings": {"slidesToShow": 1}}]}'>
				<!-- Member #1 -->
				@foreach ($founder as $item)
				<div class="member">
					<div class="member__img">
					  <img src="{{asset('assets/images/team/'.$item->img.'')}}" alt="member img">
					</div><!-- /.member-img -->
					<div class="member__info">
					  <h5 class="member__name"><a href="doctors-single-doctor1.html">{{$item->nama}}</a></h5>
					  <p class="member__job">{{$item->jabatan}}</p>
					  <p class="member__desc">{{$item->caption}}</p>
					  <div class="mt-20 d-flex flex-wrap justify-content-between align-items-center">
						{{-- <a href="doctors-single-doctor1.html" class="btn btn__secondary btn__link btn__rounded">
						  <span>Konsultasikan</span>
						  <i class="icon-arrow-right"></i>
						</a> --}}
						<ul class="social-icons list-unstyled mb-0">
						  <li><a href="{{$item->link_facebook}}" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
						  <li><a href="{{$item->link_twitter}}" class="twitter"><i class="fab fa-twitter"></i></a></li>
						  <li><a href="{{$item->link_instagram}}" class="instagram"><i class="fab fa-instagram"></i></a></li>
						</ul><!-- /.social-icons -->
					  </div>
					</div><!-- /.member-info -->
				  </div><!-- /.member -->
				@endforeach
				
				<!-- Member #4 -->
			  </div><!-- /.carousel -->
			</div><!-- /.col-12 -->
		  </div><!-- /.row -->
		</div><!-- /.container -->
	  </section><!-- /.Team -->
  
	  <!-- ======================
	   Work Process 
	  ========================= -->
	  <section class="work-process work-process-carousel pt-130 pb-0 bg-overlay bg-overlay-secondary">
		<div class="bg-img"><img src="{{asset('assets/images/banners/'.$web_setting->banner_section_6.'')}}" alt="background"></div>
		<div class="container">
		  <div class="row heading-layout2">
			<div class="col-12">
			  {{-- <h2 class="heading__subtitle color-primary">Caring For The Health Of You And Your Family.</h2> --}}
			</div><!-- /.col-12 -->
			<div class="col-sm-12 col-md-12 col-lg-6 col-xl-5">
			  <h3 class="heading__title color-white">{{$content_section_6->title}}
			  </h3>
			</div><!-- /.col-xl-5 -->
			<div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 offset-xl-1">
			  <p class="heading__desc font-weight-bold color-gray mb-40">{{$content_section_6->caption}}
			  </p>
			  {{-- <ul class="list-items list-items-layout2 list-items-light list-horizontal list-unstyled">
				<li>Menjadi Pendengar yang Baik</li>
				<li>Memberikan Pendamping yang Kompeten</li>
				<li>Menjaga Kelestarian Alam</li>
				<li>Berdaya di Masyarakat</li>
			  </ul> --}}
			</div><!-- /.col-xl-6 -->
		  </div><!-- /.row -->
		  <div class="row">
			<div class="col-12">
			  {{-- <div class="carousel-container mt-90"> --}}
				<div class="slick-carousel"
				  data-slick='{"slidesToShow": 3, "slidesToScroll": 1, "infinite":false, "arrows": true, "dots": false, "responsive": [{"breakpoint": 1200, "settings": {"slidesToShow": 3}}, {"breakpoint": 992, "settings": {"slidesToShow": 2}}, {"breakpoint": 767, "settings": {"slidesToShow": 2}}, {"breakpoint": 480, "settings": {"slidesToShow": 1}}]}'>
				  <!-- process item #1 -->
				  @php
					  $detail = CaptionDetail::where('id_landing_page',$content_section_6->id)->get();
				  @endphp
				  @foreach ($detail as $item)
				  <div class="process-item">
					<span class="process__number">#</span>
					<div class="process__icon">
					  <i class="{{$item->icon}}"></i>
					</div><!-- /.process__icon -->
					@if ($item->title_detail != "Berdaya di Masyarakat")
						<h4 class="process__title">{{$item->title_detail}}</h4>
						<p class="process__desc">{{$item->caption1}}</p>
						<a href="https://api.whatsapp.com/send?phone={{ $header_setting->no_telp }}" class="btn btn__secondary btn__link">
						<span>Konsultasikan</span>
						<i class="icon-arrow-right"></i>
						</a>
					@else
						<h4 class="process__title">{{$item->title_detail}}</h4>
						<p class="process__desc">{{$item->caption1}}</p>
						<a href="https://api.whatsapp.com/send?phone={{ $header_setting->no_telp }}" class="btn btn__secondary btn__link">
						<span>Konsultasikan</span>
						<i class="icon-arrow-right"></i>
						</a>
						<div class="mb-20"></div>
					@endif
					
				  </div><!-- /.process-item -->
				  @endforeach
				 
				  {{-- <!-- process-item #2 -->
				  <div class="process-item">
					<span class="process__number">02</span>
					<div class="process__icon">
					  <i class="icon-dna"></i>
					</div><!-- /.process__icon -->
					<h4 class="process__title">Memberikan Pendamping yang Kompeten</h4>
					<p class="process__desc">Program Kampoong hening memberikan keuntungan bagi pendamping yang menjadi penolong handal berkontribusi bagi siapapun yang membutuhkan, juga menjadi sumber finansial/pemasukan </p>
					<a href="https://api.whatsapp.com/send?phone={{ $header_setting->no_telp }}" class="btn btn__secondary btn__link">
					  <span>Konsultasikan</span>
					  <i class="icon-arrow-right"></i>
					</a>
				  </div><!-- /.process-item -->
				  <!-- process-item #3 -->
				  <div class="process-item">
					<span class="process__number">03</span>
					<div class="process__icon">
					  <i class="icon-medicine"></i>
					</div><!-- /.process__icon -->
					<h4 class="process__title">Menjaga Kelestarian Alam</h4>
					<p class="process__desc">Program yang disajikan Kampoong Hening berdampak bagi alam, karena setiap individu terdorong untuk menjaga alam, melestarikan, menjadikannya bukan sebagai obyek, melainkan sebagai mahluk hidup lainnya yang disayangi, diperhatikan dan berkolaborasi</p>
					<a href="https://api.whatsapp.com/send?phone={{ $header_setting->no_telp }}" class="btn btn__secondary btn__link">
					  <span>Konsultasikan</span>
					  <i class="icon-arrow-right"></i>
					</a>
				  </div><!-- /.process-item -->
				  <!-- process-item #4 -->
				  <div class="process-item">
					<span class="process__number">04</span>
					<div class="process__icon">
					  <i class="icon-stethoscope"></i>
					</div><!-- /.process__icon -->
					<h4 class="process__title">Berdaya di Masyarakat</h4>
					<p class="process__desc">Dengan program sosial yang diselenggarakan oleh para pendamping yaitu trainer, konselor dan ecohealing therapist yang memiliki tugas memberikan kebaikan bagi masyarakat sehat mental dan peningkatan finansial.</p>
					<a href="https://api.whatsapp.com/send?phone={{ $header_setting->no_telp }}" class="btn btn__secondary btn__link">
					  <span>Konsultasikan</span>
					  <i class="icon-arrow-right"></i>
					</a>
				  </div><!-- /.process-item -->
				  <div class="process-item">
					<span class="process__number">01</span>
					<div class="process__icon">
					  <i class="icon-health-report"></i>
					</div><!-- /.process__icon -->
					<h4 class="process__title">Menjadi Pendengar yang Baik</h4>
					<p class="process__desc">Program Kampoong Hening disajikan bagi sahabat yang ingin hidupnya jadi lebih baik, merilis luka batin dan menjadi resilience menghadapi tantangan mencapai misi dalam hidupnya dengan bahagia</p>
					<a href="https://api.whatsapp.com/send?phone={{ $header_setting->no_telp }}" class="btn btn__secondary btn__link">
					  <span>Konsultasikan</span>
					  <i class="icon-arrow-right"></i>
					</a>
				  </div><!-- /.process-item -->
				  <!-- process-item #2 -->
				  <div class="process-item">
					<span class="process__number">02</span>
					<div class="process__icon">
					  <i class="icon-dna"></i>
					</div><!-- /.process__icon -->
					<h4 class="process__title">Memberikan Pendamping yang Kompeten</h4>
					<p class="process__desc">Program Kampoong hening memberikan keuntungan bagi pendamping yang menjadi penolong handal berkontribusi bagi siapapun yang membutuhkan, juga menjadi sumber finansial/pemasukan </p>
					<a href="https://api.whatsapp.com/send?phone={{ $header_setting->no_telp }}" class="btn btn__secondary btn__link">
					  <span>Konsultasikan</span>
					  <i class="icon-arrow-right"></i>
					</a>
				  </div><!-- /.process-item -->
				  <!-- process-item #3 -->
				  <div class="process-item">
					<span class="process__number">03</span>
					<div class="process__icon">
					  <i class="icon-medicine"></i>
					</div><!-- /.process__icon -->
					<h4 class="process__title">Menjaga Kelestarian Alam</h4>
					<p class="process__desc">Program yang disajikan Kampoong Hening berdampak bagi alam, karena setiap individu terdorong untuk menjaga alam, melestarikan, menjadikannya bukan sebagai obyek, melainkan sebagai mahluk hidup lainnya yang disayangi, diperhatikan dan berkolaborasi</p>
					<a href="https://api.whatsapp.com/send?phone={{ $header_setting->no_telp }}" class="btn btn__secondary btn__link">
					  <span>Konsultasikan</span>
					  <i class="icon-arrow-right"></i>
					</a>
				  </div><!-- /.process-item -->
				  <!-- process-item #4 -->
				  <div class="process-item">
					<span class="process__number">04</span>
					<div class="process__icon">
					  <i class="icon-stethoscope"></i>
					</div><!-- /.process__icon -->
					<h4 class="process__title">Berdaya di Masyarakat</h4>
					<p class="process__desc">Dengan program sosial yang diselenggarakan oleh para pendamping yaitu trainer, konselor dan ecohealing therapist yang memiliki tugas memberikan kebaikan bagi masyarakat sehat mental dan peningkatan finansial.</p>
					<a href="https://api.whatsapp.com/send?phone={{ $header_setting->no_telp }}" class="btn btn__secondary btn__link">
					  <span>Konsultasikan</span>
					  <i class="icon-arrow-right"></i>
					</a>
				  </div><!-- /.process-item -->
				  <!-- process-item #5 --> --}}
				  
				</div><!-- /.carousel -->
			  {{-- </div> --}}
			</div><!-- /.col-12 -->
		  </div><!-- /.row -->
		</div><!-- /.container -->
		<div class="cta bg-light-blue">
		  <div class="container">
			<div class="row align-items-center">
			  <div class="col-sm-12 col-md-2 col-lg-2">
				<img src="{{asset('assets/images/icons/alert2.png')}}" alt="alert">
			  </div><!-- /.col-lg-2 -->
			  <div class="col-sm-12 col-md-7 col-lg-7">
				<h4 class="cta__title">{{$content_section_7->title}}</h4>
				<p class="cta__desc">{{$content_section_7->caption}}
				</p>
			  </div><!-- /.col-lg-7 -->
			  <div class="col-sm-12 col-md-3 col-lg-3">
				<a href="https://api.whatsapp.com/send?phone={{ $header_setting->no_telp }}" class="btn btn__primary btn__secondary-style2 btn__rounded" style="margin-left: 30% !important;">
				  <span>Kontak Kami</span>
				  <i class="icon-arrow-right"></i>
				</a>
			  </div><!-- /.col-lg-3 -->
			</div><!-- /.row -->
		  </div><!-- /.container -->
		</div><!-- /.cta -->
	  </section><!-- /.Work Process -->
  
	  <!-- ========================= 
		Testimonials layout 2
		=========================  -->
	  <section class="testimonials-layout2 pt-130 pb-40">
		<div class="container">
		  <div class="testimonials-wrapper">
			<div class="row">
			  <div class="col-sm-12 col-md-12 col-lg-5">
				<div class="heading-layout2">
				  <h3 class="heading__title">Cerita dan kisah mereka tentang kampoong hening</h3>
				</div><!-- /.heading -->
			  </div><!-- /.col-lg-5 -->
			  <div class="col-sm-12 col-md-12 col-lg-7">
				<div class="slider-with-navs">
				  <!-- Testimonial #1 -->
				  @foreach ($testimoni as $item)
				  <div class="testimonial-item">
					<h3 class="testimonial__title" style="text-align: justify; text-transform: none !important;">"{!!$item->caption!!}"
					</h3>
				  </div><!-- /. testimonial-item -->
				  @endforeach
				  
				  <!-- Testimonial #2 -->
				</div><!-- /.slick-carousel -->
				<div class="slider-nav mb-60">
					@foreach ($testimoni as $item)
					<div class="testimonial__meta">
						<div class="testimonial__thmb">
							@if ($item->img != null)
							<img src="{{asset('assets/images/testimonials/thumbs/'.$item->img.'')}}" alt="author thumb">
							@else 
							<img src="{{asset('assets/images/testimonials/thumbs/1.png')}}" alt="author thumb">
							@endif
						</div><!-- /.testimonial-thumb -->
						<div>
						  <h4 class="testimonial__meta-title">{{$item->nama}}</h4>
						  <p class="testimonial__meta-desc">{{$item->jabatan}}</p>
						</div>
					  </div><!-- /.testimonial-meta -->
					@endforeach
				 

				</div><!-- /.slider-nav -->
			  </div><!-- /.col-lg-7 -->
			</div><!-- /.row -->
		  </div><!-- /.testimonials-wrapper -->
		</div><!-- /.container -->
	  </section><!-- /.testimonials layout 2 -->
  
	  <!-- ========================
		 gallery
		=========================== -->
	  <section class="gallery pt-0 pb-90">
		{{-- <div class="container"> --}}
		  <div class="row">
			<div class="col-12">
			  <div class="slick-carousel"
				data-slick='{"slidesToShow": 4, "slidesToScroll": 1, "autoplay": true, "arrows": true, "dots": false, "responsive": [ {"breakpoint": 992, "settings": {"slidesToShow": 2}}, {"breakpoint": 767, "settings": {"slidesToShow": 2}}, {"breakpoint": 480, "settings": {"slidesToShow": 1}}]}'>
				@foreach ($gallery as $item)
					<a class="popup-gallery-item" href="{{asset('assets/images/gallery/'.$item->img.'')}}">
					<img src="{{asset('assets/images/gallery/'.$item->img.'')}}" alt="gallery img">
				  </a>
				@endforeach
				
			  </div><!-- /.gallery-images-wrapper -->
			</div><!-- /.col-xl-5 -->
		  </div><!-- /.row -->
		{{-- </div><!-- /.container --> --}}
	  </section><!-- /.gallery 2 -->
  
	  <!-- ==========================
		  contact layout 3
	  =========================== -->
	  <section class="contact-layout3 bg-overlay bg-overlay-primary-gradient pb-60">
		<div class="bg-img"><img src="{{asset('assets/images/banners/'.$web_setting->banner_section_8.'')}}" alt="banner"></div>
		<div class="container">
		  <div class="row">
			<div class="col-sm-12 col-md-12 col-lg-7">
			  <div class="contact-panel mb-50">
				@if($errors->any())
				<div class="alert alert-info" role="alert" id="success">
					Data Berhasil Dikirim
				</div>
				@endif
				<form class="contact-panel__form" method="post" action="{{route('janji')}}">
					@csrf
				  <div class="row">
					<div class="col-sm-12">
					  <h4 class="contact-panel__title">Pesan reservasi anda</h4>
					  <p class="contact-panel__desc mb-30">Silahkan isi data diri anda untuk reservasi
					  </p>
					</div>
					<div class="col-sm-6 col-md-6 col-lg-6">
					  <div class="form-group">
						<i class="icon-news form-group-icon"></i>
						<input type="text" class="form-control" placeholder="Nama" id="contact-name" name="nama"
						  required>
					  </div>
					</div><!-- /.col-lg-6 -->
					<div class="col-sm-6 col-md-6 col-lg-6">
					  <div class="form-group">
						<i class="icon-email form-group-icon"></i>
						<input type="email" class="form-control" placeholder="Email" id="contact-email"
						  name="email" required>
					  </div>
					</div><!-- /.col-lg-6 -->
					<div class="col-sm-4 col-md-4 col-lg-4">
					  <div class="form-group">
						<i class="icon-phone form-group-icon"></i>
						<input type="text" class="form-control" placeholder="No telepon" id="contact-Phone"
						  name="no_telp" required>
					  </div>
					</div><!-- /.col-lg-4 -->
					<div class="col-sm-4 col-md-4 col-lg-4">
					  <div class="form-group form-group-date">
						<i class="icon-calendar form-group-icon"></i>
						<input type="date" class="form-control" id="contact-date" name="tanggal" required>
					  </div>
					</div><!-- /.col-lg-4 -->
					<div class="col-sm-4 col-md-4 col-lg-4">
					  <div class="form-group form-group-date">
						<i class="icon-clock form-group-icon"></i>
						<input type="time" class="form-control" id="contact-time" name="waktu" required>
					  </div>
					</div><!-- /.col-lg-4 -->
					<div class="col-12">
					  <button type="submit" class="btn btn__secondary btn__rounded btn__block btn__xhight mt-10">
						<span>Buat Janji</span> <i class="icon-arrow-right"></i>
					  </button>
					  {{-- <div class="contact-result"></div> --}}
					</div><!-- /.col-lg-12 -->
				  </div><!-- /.row -->
				</form>
			  </div>
			</div><!-- /.col-lg-7 -->
			<div class="col-sm-12 col-md-12 col-lg-5">
			  <div class="heading heading-light mb-30">
				<h3 class="heading__title mb-30">Menyenangkan bisa berkontribusi untuk temukan diri Anda yang baru
				</h3>
				{{-- <p class="heading__desc"> strives to make each interaction with patients clear, Our staffconcise, and
				  inviting. Support the important work of Medicsh Hospital by making a much-needed donation today.
				</p> --}}
			  </div>
			  {{-- <div class="d-flex align-items-center">
				<a href="contact-us.html" class="btn btn__white btn__rounded mr-30">
				  <i class="fas fa-heart"></i> <span>Make A Gift</span>
				</a>
				<a class="video__btn video__btn-white popup-video" href="https://www.youtube.com/embed/3Q3mQNodgyg">
				  <div class="video__player">
					<i class="fa fa-play"></i>
				  </div>
				  <span class="video__btn-title color-white">Play Video</span>
				</a>
			  </div> --}}
			  <div class="text__block">
				<p class="text__block-desc color-white font-weight-bold">Sudah banyak yang bangkit dari keterpurukan untuk bisa menjadi dirinya yang baru dan lebih siap untuk menghadapi dunia. Siapkah Anda mengikuti jejak mereka?
				</p>
				<div class="sinature color-white">
				  {{-- <span class="font-weight-bold">Martin Qube</span><span>, Medcity Manager</span> --}}
				</div>
			  </div><!-- /.text__block -->
			  {{-- <div class="slick-carousel clients-light mt-20"
				data-slick='{"slidesToShow": 3, "arrows": false, "dots": false, "autoplay": true,"autoplaySpeed": 2000, "infinite": true, "responsive": [ {"breakpoint": 992, "settings": {"slidesToShow": 3}}, {"breakpoint": 767, "settings": {"slidesToShow": 2}}, {"breakpoint": 480, "settings": {"slidesToShow": 2}}]}'>
				<div class="client">
				  <img src="{{asset('assets/images/backgrounds/1.jpg')}}" alt="client">
				  <img src="{{asset('assets/images/clients/1.png')}}" alt="client">
				</div><!-- /.client -->
				<div class="client">
				  <img src="{{asset('assets/images/clients/2.png')}}" alt="client">
				  <img src="{{asset('assets/images/clients/2.png')}}" alt="client">
				</div><!-- /.client -->
				<div class="client">
				  <img src="{{asset('assets/images/clients/3.png')}}" alt="client">
				  <img src="{{asset('assets/images/clients/3.png')}}" alt="client">
				</div><!-- /.client -->
				<div class="client">
				  <img src="{{asset('assets/images/clients/4.png')}}" alt="client">
				  <img src="{{asset('assets/images/clients/4.png')}}" alt="client">
				</div><!-- /.client -->
				<div class="client">
				  <img src="{{asset('assets/images/clients/5.png')}}" alt="client">
				  <img src="{{asset('assets/images/clients/5.png')}}" alt="client">
				</div><!-- /.client -->
				<div class="client">
				  <img src="{{asset('assets/images/clients/6.png')}}" alt="client">
				  <img src="{{asset('assets/images/clients/6.png')}}" alt="client">
				</div><!-- /.client -->
				<div class="client">
				  <img src="{{asset('assets/images/clients/7.png')}}" alt="client">
				  <img src="{{asset('assets/images/clients/7.png')}}" alt="client">
				</div><!-- /.client -->
			  </div><!-- /.carousel --> --}}
			</div><!-- /.col-lg-5 -->
		  </div><!-- /.row -->
		</div><!-- /.container -->
	  </section><!-- /.contact layout 3 -->
  
	  <!-- ======================
		Blog Grid
	  ========================= -->
	  <section class="blog-grid mt-20 pb-10">
		<div class="container">
		  <div class="row">
			<div class="col-sm-12 col-md-12 col-lg-6 offset-lg-3">
			  <div class="heading text-center mb-40">
				<h3 class="heading__title">Artikel Terbaru</h3>
			  </div><!-- /.heading -->
			</div><!-- /.col-lg-6 -->
		  </div><!-- /.row -->
		  <div class="row">
			<!-- Post Item #1 -->
			@foreach ($artikel as $item)
			<div class="col-sm-12 col-md-6 col-lg-4">
				<div class="post-item">
				  <div class="post__img">
					<a href="{{$item->guid}}">
						@php
							$image = DB::table('wp_posts')->where('post_parent',$item->ID)->where('post_type','attachment')->first();
							$img = null;
							if(isset($image->guid))
							{
								$img = $image->guid;
							}
						@endphp
						@if (!empty($img))
							<img src="{{$img}}" alt="post image" loading="lazy">
						@else
							<img src="{{asset('assets/images/blog/grid/1.jpg')}}" alt="post image" loading="lazy">
						@endif
					</a>
				  </div><!-- /.post__img -->
				  <div class="post__body">
					<div class="post__meta-cat">
					  <a href="#">{{$item->display_name}}</a>
					</div><!-- /.blog-meta-cat -->
					<div class="post__meta d-flex">
					  <span class="post__meta-date">{{$item->post_date }}</span>
					  <a class="post__meta-author" href="{{$item->guid}}">{{$item->display_name}}</a>
					</div>
					<h4 class="post__title"><a href="{{$item->guid}}">{{$item->post_title}}</a></h4>
	                
					<p class="post__desc">
						@php
							$caption = substr($item->post_content, 0, 200);
							$caption = strip_tags($caption);
						@endphp
						{{$caption}}
					</p>
					
					<a href="{{$item->guid}}" class="btn btn__secondary btn__link btn__rounded">
					  <span>Lebih Lanjut</span>
					</a>
				  </div><!-- /.post__body -->
				</div><!-- /.post-item -->
			  </div><!-- /.col-lg-4 -->
			@endforeach
			
			<!-- Post Item #2 -->
			
			<!-- Post Item #3 -->
		  </div><!-- /.row -->
		</div><!-- /.container -->
	  </section><!-- /.blog Grid -->
  
	<section class="blog-grid pb-10">
	  <div class="container">
		  <div class="heading text-center mb-40" style="justify-content: center;">
			<h3 class="heading__title">Instagram</h3>
		  </div><!-- /.heading -->
		
	  <div class="row">
		@foreach ($instagram as $item)
		<div class="col-md-4">
			<div class="thumbnail">
			  <a href="{{$item->permalink}}">
				<div class="caption">
					{{-- <h6>{{$item->caption}}</h6> --}}
				  </div>
				@if ($item->media_type == 'VIDEO')
					<iframe style="width: 100%; height: 350px;" src="https://video-lax3-2.cdninstagram.com/v/t50.16885-16/10000000_335410448021939_1822740817941155220_n.mp4?_nc_cat=111&vs=448185413324767_2415002295&_nc_vs=HBksFQAYJEdJQ1dtQUN6dVp2VURURUJBSlROLU5vZHJVc1pidlZCQUFBRhUAAsgBABUAGCRHQ3ZxMUFfUW5mNUUwWDBEQUt2VXRWQ1dNVUYwYnZWQkFBQUYVAgLIAQAoABgAGwGIB3VzZV9vaWwBMRUAACaU3PSD4LjfPxUCKAJDMywXQGSx41P3ztkYEmRhc2hfYmFzZWxpbmVfMV92MREAdewHAA%3D%3D&ccb=1-5&_nc_sid=59939d&efg=eyJ2ZW5jb2RlX3RhZyI6InZ0c192b2RfdXJsZ2VuLjEyODAuaWd0diJ9&_nc_ohc=vkRRN2TKel0AX_snI41&_nc_ht=video-lax3-2.cdninstagram.com&edm=ANQ71j8EAAAA&oh=00_AT_CS3m58vH8bJ1PsNRnyd294LZQv5_LtsIwRqMJJejxDA&oe=61BE39CE&_nc_rid=2e0b626454" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				@else
					<img src="{{$item->media_url}}" alt="Lights" style="width:100%" class="mb-30"> 
				@endif
			  </a>
			</div>
		  </div>
		@endforeach
		
	  </div>
	</div><!-- /.container -->
  </section><!-- /.blog Grid -->
  
	  <!-- ======================
		Blog Grid
	  ========================= -->
	  <section class="blog-grid pb-10">
		<div class="container">
		  <div class="row">
			<div class="col-sm-12 col-md-12 col-lg-6 offset-lg-3">
			  <div class="heading text-center mb-40">
				<h3 class="heading__title">Youtube</h3>
			  </div><!-- /.heading -->
			</div><!-- /.col-lg-6 -->
		  </div><!-- /.row -->
		  <div class="row">
			<!-- Post Item #1 -->
			<div class="col-sm-12 col-md-12 col-lg-12">
			  <div class="post-item">
				<div class="post__img">
				<iframe style="width: 100%; height: 600px;" src="https://www.youtube.com/embed/3Q3mQNodgyg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div><!-- /.post__img -->
				<div class="post__body">
				</div><!-- /.post__body -->
			  </div><!-- /.post-item -->
			</div><!-- /.col-lg-4 -->
			<!-- Post Item #2 -->
			<!-- Post Item #3 -->
		  </div><!-- /.row -->
		</div><!-- /.container -->
	  </section><!-- /.blog Grid -->
	  <section class="contact-layout3 bg-overlay bg-overlay-primary-gradient pb-0 pt-0">
		<div class="cta bg-light-blue">
			<br>
			<div class="container">
			  <div class="row align-items-center">
				{{-- <div class="col-sm-12 col-md-2 col-lg-2">
				  <img src="{{asset('assets/images/icons/alert2.png')}}" alt="alert">
				</div><!-- /.col-lg-2 --> --}}
				<div class="col-sm-6 col-md-6 col-lg-6">
				  <h4 class="cta__title" style="color: white">{{$content_section_11->title}}</h4>
				  <p class="cta__desc" style="color: white;">{{$content_section_11->caption}}
				  </p>
				</div><!-- /.col-lg-7 -->
				<div class="col-sm-6 col-md-6 col-lg-6">
				  <a href="{{ $content_section_11->no_telp }}" class="btn btn__primary btn__secondary-style2 btn__rounded" style="margin-left: 70% !important;">
					<span>Klik Disini</span>
					<i class="icon-arrow-right"></i>
				  </a>
				</div><!-- /.col-lg-3 -->
			  </div><!-- /.row -->
			</div><!-- /.container -->
			<br>
		  </div><!-- /.cta -->
	</section><!-- /.blog Grid -->
	  <!-- ======================
		Blog Grid
	  ========================= -->
	  {{-- <section class="blog-grid pb-50" style="background-color: white;">
		<div class="container">
		  <div class="row">
			<div class="col-sm-12 col-md-12 col-lg-6 offset-lg-3">
			  <div class="heading text-center mb-40">
				<h3 class="heading__title">Google Maps</h3>
			  </div><!-- /.heading -->
			</div><!-- /.col-lg-6 -->
		  </div><!-- /.row -->
		  <div class="row">
			<!--Google map-->
			<!--Google map-->
			<div id="map-container-google-2" class="z-depth-1-half map-container col-sm-12 col-md-12 col-lg-12" style="height: 500px;">
			  <iframe src="https://www.google.com/maps?q=kampoong%20hening&z=14&t=&ie=UTF8&output=embed" frameborder="0"
				style="border:0; width: 100%; height: 100%;" allowfullscreen></iframe>
			</div>
  <!--Google Maps-->
  
  <!--Google Maps-->
		  </div>
		</div><!-- /.container -->
	  </section><!-- /.blog Grid --> --}}
@endsection
