@extends('layouts.dashboard')
@section('dashboard-favicon')
    <link rel="icon" href="{{asset('assets/images/favicon/'.$web_setting->favicon.'')}}" type="image/x-icon">
@endsection
@section('bearcrumb')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5 class="m-b-10">Web Setting</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#!">Web Setting</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="alert alert-info" role="alert" id="success">
    Data Berhasil Disimpan
</div>
<div class="row">
    <!-- prject ,team member start -->
    <div class="col-xl-12 col-md-12">
        <div class="card table-card">
            <div class="card-header">
                <h3>Web Setting</h3>
            </div>
            <div class="card-body p-0">
                <div class="container-fluid">
                    <form class="form-group" method="POST" action="{{route('sync-instagram')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="row mb-3">
                            <div class="form-group col-xl-8 col-md-8  mt-3">
                                <label for="exampleFormControlTextarea1"><b> Token API Instagram</b></label>
                                <input type="text" class="form-control" aria-describedby="alamat" name="token" placeholder="Token API" value="{{ $web_setting->token }}">
                                <small id="alamat" class="form-text text-muted">Token API Instagram</small>
                                {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                            </div>
                            <div class="form-group col-xl-4 col-md-4 mt-5">
                                <button type="submit" class="btn  btn-primary mb-2">Sync Instagram</button>
                            </div>
                        </div>
                    </form>
                    <form class="form-group" method="POST" action="{{route('save-web-setting')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="row mb-3">
                            <div class="form-group col-xl-10 col-md-10  mt-3">
                                <label for="exampleFormControlTextarea1"><b> Title Web</b></label>
                                <input type="text" class="form-control" id="title" name="title" aria-describedby="title" placeholder="Masukan Title" value="{{ $web_setting->title }}">
                                <small id="title" class="form-text text-muted">Ubah title sesuai kebutuhan</small>
                                {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="form-group col-xl-10 col-md-10  mt-3">
                                <label for="exampleFormControlTextarea1"><b> Alamat</b></label>
                                <input type="text" class="form-control" id="alamat" name="alamat" aria-describedby="alamat" placeholder="Masukan alamat" value="{{ $web_setting->alamat }}">
                                <small id="alamat" class="form-text text-muted">Ubah alamat sesuai kebutuhan</small>
                                {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-xl-3 mb-5">
                                <label for="exampleFormControlTextarea1"><b>Favicon Web</b></label>
                                <div class="card mb-3">
                                    <img class="img-thumbnail" id="preview-favicon" src="{{asset('assets/images/favicon/'.$web_setting->favicon.'')}}">
                                </div>
                                <div class="input-group">
                                    <div class="input-group">
                                        <div class="form-group">
                                            <input type="file" class="form-control-file" id="favicon" name="favicon">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-xl-3 mb-5">
                                <label for="exampleFormControlTextarea1"><b>Logo Header</b></label>
                                <div class="card mb-3">
                                    <img class="img-thumbnail" id="preview-logo" src="{{asset('assets/images/logo/'.$web_setting->logo.'')}}">
                                </div>
                                <div class="input-group">
                                    <div class="form-group">
                                        <input type="file" class="form-control-file" id="logo" name="logo">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-xl-3 mb-5">
                                <label for="exampleFormControlTextarea1"><b>Logo Footer</b></label>
                                <div class="card mb-3">
                                    <img class="img-thumbnail" id="preview-logo-footer" src="{{asset('assets/images/logo/'.$web_setting->logo_footer.'')}}">
                                </div>
                                <div class="input-group">
                                    <div class="form-group">
                                        <input type="file" class="form-control-file" id="logofooter" name="logofooter">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-xl-3 mb-5">
                                <label for="exampleFormControlTextarea1"><b>background Section 3</b></label>
                                <div class="card mb-3">
                                    <img class="img-thumbnail" id="preview-banner-3" src="{{asset('assets/images/backgrounds/'.$web_setting->banner_section_3.'')}}">
                                </div>
                                <div class="input-group">
                                    <div class="form-group">
                                        <input type="file" class="form-control-file" id="banner3" name="banner3">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-xl-3 mb-5">
                                <label for="exampleFormControlTextarea1"><b>background Section 6</b></label>
                                <div class="card mb-3">
                                    <img class="img-thumbnail" id="preview-banner-6" src="{{asset('assets/images/banners/'.$web_setting->banner_section_6.'')}}">
                                </div>
                                <div class="input-group">
                                    <div class="form-group">
                                        <input type="file" class="form-control-file" id="banner6" name="banner6">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-xl-3 mb-5">
                                <label for="exampleFormControlTextarea1"><b>background Section 8</b></label>
                                <div class="card mb-3">
                                    <img class="img-thumbnail" id="preview-banner-8" src="{{asset('assets/images/banners/'.$web_setting->banner_section_8.'')}}">
                                </div>
                                <div class="input-group">
                                    <div class="form-group">
                                        <input type="file" class="form-control-file" id="banner8" name="banner8">
                                    </div>
                                </div>
                            </div>
                            {{-- 
                            <div class="col-md-3 col-xl-3 mb-5">
                                <label for="exampleFormControlTextarea1"><b> Image Slider 2</b></label>
                                <div class="card mb-3">
                                    <img class="img-thumbnail" id="preview-slider-2" src="{{asset('dist/assets/images/slider/img-slide-3.jpg')}}">
                                </div>
                                <div class="input-group">
                                    <div class="form-group">
                                        <input type="file" class="form-control-file" id="slider_2" name="slider_2">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-xl-3 mb-5">
                                <label for="exampleFormControlTextarea1"><b> Image Slider 3</b></label>
                                <div class="card mb-3">
                                    <img class="img-thumbnail" id="preview-slider-3" src="{{asset('dist/assets/images/slider/img-slide-3.jpg')}}">
                                </div>
                                <div class="input-group">
                                    <div class="form-group">
                                        <input type="file" class="form-control-file" id="slider_3" name="slider_3">
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                        <div class="form-group col-xl-2 col-md-2">
                            <button type="submit" class="btn  btn-primary mb-2">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('custom-script')
    @if($errors->any())
        <script>
            window.addEventListener('load', function() {
            $("#success").show().delay(5000).fadeOut();
        })
        </script>
    @else
        <script>
            window.addEventListener('load', function() {
                $("#success").hide();
            })
        </script>
    @endif
    <script>
        // function onChange(file) {
        //     console.log(file)
        //     const [file] = file
        //     if (file) {
        //     blah.src = URL.createObjectURL(file)
        //     }
        // }
        favicon.onchange = function(event) {
            console.log('test')
        var previewfavicon = document.getElementById("preview-favicon");
        var fileList = favicon.files;
            if (fileList) {
                previewfavicon.src = URL.createObjectURL(fileList[0])
            }  
        }

        logo.onchange = function(event) {
        var previewlogo = document.getElementById("preview-logo");
        var fileList = logo.files;
            if (fileList) {
                previewlogo.src = URL.createObjectURL(fileList[0])
            }  
        }

        logofooter.onchange = function(event) {
        var previewlogofooter = document.getElementById("preview-logo-footer");
        var fileList = logofooter.files;
            if (fileList) {
                previewlogofooter.src = URL.createObjectURL(fileList[0])
            }  
        }

        banner6.onchange = function(event) {
        var previewBanner6 = document.getElementById("preview-banner-6");
        var fileList = banner6.files;
        console.log(fileList)
            if (fileList) {
                previewBanner6.src = URL.createObjectURL(fileList[0])
            }  
        }

        banner8.onchange = function(event) {
        var previewBanner8 = document.getElementById("preview-banner-8");
        var fileList = banner8.files;
        console.log(fileList)
            if (fileList) {
                previewBanner8.src = URL.createObjectURL(fileList[0])
            }  
        }

        banner3.onchange = function(event) {
        var previewBanner3 = document.getElementById("preview-banner-3");
        var fileList = banner3.files;
        console.log(fileList)
            if (fileList) {
                previewBanner3.src = URL.createObjectURL(fileList[0])
            }  
        }

        // slider_3.onchange = function(event) {
        // var previewSlider = document.getElementById("preview-slider-3");
        // var fileList = slider_3.files;
        // console.log(fileList)
        //     if (fileList) {
        //         previewSlider.src = URL.createObjectURL(fileList[0])
        //     }  
        // }
    </script>
@endsection