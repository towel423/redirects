@extends('layouts.dashboard')
@section('dashboard-favicon')
    <link rel="icon" href="{{asset('assets/images/favicon/'.$web_setting->favicon.'')}}" type="image/x-icon">
@endsection
@section('bearcrumb')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5 class="m-b-10">Post Testimoni</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#!">Post Testimoni</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="alert alert-info" role="alert" id="success">
    Data Berhasil Disimpan
</div>
<div class="row">
    <!-- prject ,team member start -->
    <div class="col-xl-12 col-md-12">
        <div class="card table-card">
            <div class="card-header">
                <h3>Post Testimoni</h3>
            </div>
            <div class="card-body p-0">
                <div class="container-fluid">
                    <form class="form-group" method="POST" action="{{route('add-testimoni')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="row mb-3">
                            <div class="form-group col-xl-3 col-md-6  mt-3">
                                <label for="exampleFormControlTextarea1"><b>Nama</b></label>
                                <input type="text" class="form-control" id="nama" name="nama" aria-describedby="title" placeholder="Masukan Nama">
                                <small id="title" class="form-text text-muted">Ubah nama sesuai kebutuhan</small>
                            </div>
                            <div class="form-group col-xl-3 col-md-6  mt-3">
                                <label for="exampleFormControlTextarea1"><b>Jabatan</b></label>
                                <input type="text" class="form-control" id="jabatan" name="jabatan" aria-describedby="title" placeholder="Masukan jabatan">
                                <small id="caption" class="form-text text-muted">Ubah jabatan sesuai kebutuhan</small>
                            </div>
                            <div class="col-md-3 col-xl-6 mt-3">
                                <label for="exampleFormControlTextarea1"><b>Foto</b></label>
                                <div class="input-group">
                                    <div class="input-group">
                                        <div class="form-group">
                                            <input type="file" class="form-control-file" id="foto" name="foto">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-xl-3 col-md-6  mt-3">
                                <label for="exampleFormControlTextarea1"><b>Caption</b></label>
                                {{-- <input type="text" class="form-control" id="caption" name="caption" aria-describedby="title" placeholder="Masukan Caption"> --}}
                                <textarea class="form-control" id="exampleFormControlTextarea1" name="caption" rows="4"></textarea>
                                <small id="caption" class="form-text text-muted">Ubah caption sesuai kebutuhan</small>
                            </div>
                           
                            <div class="col-md-6 col-xl-3 mt-5">
                                <div class="input-group">
                                    <div class="input-group">
                                        <div class="form-group">
                                            <button type="submit" class="btn  btn-primary mb-2">Tambah</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Jabatan</th>
                                        <th>Caption</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($testimoni->data as $item)
                                    <tr>
                                        <td>{{ $item->nama }}</td>
                                        <td>{{ $item->jabatan }}</td>
                                        <td>{{ $item->caption }}</td>
                                        <td>
                                            <a href="{{url('/manage-landingpage/testimoni/delete/'.$item->id.'')}}" class="badge badge-danger"><i class="feather icon-trash-2"></i></a>
                                            <a href="{{url('/edit/testimoni/'.$item->id.'')}}" class="badge badge-primary"><i class="feather icon-edit"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div>
                            <nav aria-label="Page navigation example" style="float: right;">
                                <ul class="pagination">
                                    <li class="page-item"><a class="page-link" href="{{ $testimoni->prev_page_url }}">Previous</a></li>
                                    @php
                                        $counter = count($testimoni->links);
                                        $i = 0;
                                    @endphp
                                    @if ($counter > 0)
                                        @foreach ($testimoni->links as $item)
                                            @if($i > 2 && $i < $counter-1)
                                                <li class="page-item"><a class="page-link" href="{{ $item->url }}">{{ $i }}</a></li>
                                            @endif
                                        @php
                                           $i++; 
                                        @endphp
                                        @endforeach
                                    @endif
                                    <li class="page-item"><a class="page-link" href="{{ $testimoni->next_page_url }}">Next</a></li>
                                </ul>
                            </nav>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('custom-script')
    @if($errors->any())
        <script>
            window.addEventListener('load', function() {
            $("#success").show().delay(5000).fadeOut();
        })
        </script>
    @else
        <script>
            window.addEventListener('load', function() {
                $("#success").hide();
            })
        </script>
    @endif
    <script>
        // function onChange(file) {
        //     console.log(file)
        //     const [file] = file
        //     if (file) {
        //     blah.src = URL.createObjectURL(file)
        //     }
        // }
        favicon.onchange = function(event) {
            console.log('test')
        var previewfavicon = document.getElementById("preview-favicon");
        var fileList = favicon.files;
            if (fileList) {
                previewfavicon.src = URL.createObjectURL(fileList[0])
            }  
        }

        logo.onchange = function(event) {
        var previewlogo = document.getElementById("preview-logo");
        var fileList = logo.files;
            if (fileList) {
                previewlogo.src = URL.createObjectURL(fileList[0])
            }  
        }

        // slider_2.onchange = function(event) {
        // var previewSlider = document.getElementById("preview-slider-2");
        // var fileList = slider_2.files;
        // console.log(fileList)
        //     if (fileList) {
        //         previewSlider.src = URL.createObjectURL(fileList[0])
        //     }  
        // }

        // slider_3.onchange = function(event) {
        // var previewSlider = document.getElementById("preview-slider-3");
        // var fileList = slider_3.files;
        // console.log(fileList)
        //     if (fileList) {
        //         previewSlider.src = URL.createObjectURL(fileList[0])
        //     }  
        // }
    </script>
@endsection