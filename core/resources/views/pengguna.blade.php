@extends('layouts.dashboard')
@section('dashboard-favicon')
    <link rel="icon" href="{{asset('assets/images/favicon/'.$web_setting->favicon.'')}}" type="image/x-icon">
@endsection
@section('bearcrumb')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5 class="m-b-10">Edit Data Pengguna</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#!">Pengguna</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
@if($errors->any())
    @if($errors->all()[0] == 'error')
        <div class="alert alert-danger" role="alert" id="success">
            @php
                echo $errors->all()[1];
            @endphp
        </div>
    @else
    <div class="alert alert-info" role="alert" id="success">
        @php
            echo $errors->all()[1];
        @endphp
    </div>
    @endif
@endif
<div class="row">
    <!-- prject ,team member start -->
    <div class="col-xl-12 col-md-12">
        <div class="card table-card">
            <div class="card-header">
                <h3>Edit Data Pengguna</h3>
            </div>
            <div class="card-body p-0">
                <div class="container-fluid">
                    <form class="form-group" method="POST" action="{{route('edit-pengguna')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="row mb-3">
                            <div class="form-group col-xl-6 col-md-6  mt-3">
                                <label for="exampleFormControlTextarea1"><b>Email</b></label>
                                <input type="text" class="form-control" id="email" name="email" aria-describedby="email" placeholder="Masukan email" value="{{$pengguna->email}}" readonly>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="form-group col-xl-6 col-md-6  mt-3">
                                <label for="exampleFormControlTextarea1"><b>New Password</b></label>
                                <input type="text" class="form-control" id="password" name="password" aria-describedby="password" placeholder="Masukan Password">
                                <small id="title" class="form-text text-muted">Ubah password sesuai kebutuhan</small>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="form-group col-xl-6 col-md-6  mt-3">
                                <label for="exampleFormControlTextarea1"><b>Confirm Password</b></label>
                                <input type="text" class="form-control" id="cpassword" name="cpassword" aria-describedby="cpassword" placeholder="Masukan Konfirmasi Password">
                                <small id="title" class="form-text text-muted">Ubah password sesuai kebutuhan</small>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6 col-xl-6 mt-4">
                                <div class="input-group">
                                    <div class="input-group">
                                        <div class="form-group">
                                            <button type="submit" class="btn  btn-primary mb-2">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('custom-script')
    {{-- @if($errors->any())
        <script>
            window.addEventListener('load', function() {
            $("#success").show().delay(5000).fadeOut();
        })
        </script>
    @else
        <script>
            window.addEventListener('load', function() {
                $("#success").hide();
            })
        </script>
    @endif --}}
    <script>
        // function onChange(file) {
        //     console.log(file)
        //     const [file] = file
        //     if (file) {
        //     blah.src = URL.createObjectURL(file)
        //     }
        // }
        favicon.onchange = function(event) {
            console.log('test')
        var previewfavicon = document.getElementById("preview-favicon");
        var fileList = favicon.files;
            if (fileList) {
                previewfavicon.src = URL.createObjectURL(fileList[0])
            }  
        }

        logo.onchange = function(event) {
        var previewlogo = document.getElementById("preview-logo");
        var fileList = logo.files;
            if (fileList) {
                previewlogo.src = URL.createObjectURL(fileList[0])
            }  
        }

        // slider_2.onchange = function(event) {
        // var previewSlider = document.getElementById("preview-slider-2");
        // var fileList = slider_2.files;
        // console.log(fileList)
        //     if (fileList) {
        //         previewSlider.src = URL.createObjectURL(fileList[0])
        //     }  
        // }

        // slider_3.onchange = function(event) {
        // var previewSlider = document.getElementById("preview-slider-3");
        // var fileList = slider_3.files;
        // console.log(fileList)
        //     if (fileList) {
        //         previewSlider.src = URL.createObjectURL(fileList[0])
        //     }  
        // }
    </script>
@endsection