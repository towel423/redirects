@extends('layouts.dashboard')
@section('dashboard-favicon')
    <link rel="icon" href="{{asset('assets/images/favicon/'.$web_setting->favicon.'')}}" type="image/x-icon">
@endsection
@section('bearcrumb')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5 class="m-b-10">Jadwal Reservasi</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#!">Jadwal Reservasi</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
@section('custom-script')
{{-- @if (isset($message)) --}}

@if($errors->any())
    <script>
        window.addEventListener('load', function() {
        $("#success").show().delay(5000).fadeOut();
    })
    </script>
@else
    <script>
        window.addEventListener('load', function() {
            $("#success").hide();
        })
    </script>
@endif
@endsection
@section('content')
<div class="alert alert-info" role="alert" id="success">
    Success
</div>
<div class="row">
    <!-- prject ,team member start -->
    <div class="col-xl-12 col-md-12">
        <div class="card table-card">
            <div class="card-header">
                <h3>Jadwal Reservasi</h3>
            </div>
            <div class="card-header">
                <h4><b>Jadwal</b></h4>
            </div>
            <div class="card-body p-0">
                <div class="container-fluid">
        
                        @csrf
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>No Telp</th>
                                        <th>Tanggal</th>
                                        <th>Waktu</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($reservasi->data as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->nama }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>{{ $item->no_telp }}</td>
                                        <td>{{ $item->tanggal }}</td>
                                        <td>{{ $item->waktu }}</td>
                                        {{-- <td>
                                            <a href="{{url('/manage-landingpage/slider/delete/'.$item->id.'')}}" class="badge badge-danger"><i class="feather icon-trash-2"></i></a>
                                        </td> --}}
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div>
                            <nav aria-label="Page navigation example" style="float: right;">
                                <ul class="pagination">
                                    <li class="page-item"><a class="page-link" href="{{ $reservasi->prev_page_url }}">Previous</a></li>
                                    @php
                                        $counter = count($reservasi->links);
                                        $i = 0;
                                    @endphp
                                    @if ($counter > 0)
                                        @foreach ($reservasi->links as $item)
                                            @if($i > 2 && $i < $counter-1)
                                                <li class="page-item"><a class="page-link" href="{{ $item->url }}">{{ $i }}</a></li>
                                            @endif
                                        @php
                                           $i++; 
                                        @endphp
                                        @endforeach
                                    @endif
                                    <li class="page-item"><a class="page-link" href="{{ $reservasi->next_page_url }}">Next</a></li>
                                </ul>
                            </nav>
                        </div>
                    
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection