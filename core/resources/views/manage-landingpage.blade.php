@extends('layouts.dashboard')
@section('dashboard-favicon')
    <link rel="icon" href="{{asset('assets/images/favicon/'.$web_setting->favicon.'')}}" type="image/x-icon">
@endsection
@section('bearcrumb')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5 class="m-b-10">Kelola Landing Page</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#!">Kelola Landing Page</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
@section('custom-script')
{{-- @if (isset($message)) --}}

@if($errors->any())
    <script>
        window.addEventListener('load', function() {
        $("#success").show().delay(5000).fadeOut();
    })
    </script>
@else
    <script>
        window.addEventListener('load', function() {
            $("#success").hide();
        })
    </script>
@endif
@endsection
@section('content')
<div class="alert alert-info" role="alert" id="success">
    Success
</div>
<div class="row">
    <!-- prject ,team member start -->
    <div class="col-xl-12 col-md-12">
        <div class="card table-card">
            <div class="card-header">
                <h3>Kelola Landing Page</h3>
            </div>
            <div class="card-header">
                <h4><b>Section 1</b></h4>
            </div>
            <div class="card-body p-0">
                <div class="container-fluid">
                    <form class="form-group" method="POST" action="{{ route('add-slider') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="row mb-3">
                            <div class="form-group col-xl-3 col-md-6  mt-3">
                                <label for="exampleFormControlTextarea1"><b> Title Slider</b></label>
                                <input type="text" class="form-control" id="title" name="title" aria-describedby="title" placeholder="Masukan Title">
                                <small id="title" class="form-text text-muted">Ubah title sesuai kebutuhan</small>
                                {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                            </div>
                            <div class="form-group col-xl-3 col-md-6  mt-3">
                                <label for="exampleFormControlTextarea1"><b> Caption Slider</b></label>
                                <input type="text" class="form-control" id="caption" name="caption" aria-describedby="title" placeholder="Masukan Caption">
                                <small id="caption" class="form-text text-muted">Ubah caption sesuai kebutuhan</small>
                                {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                            </div>
                            <div class="col-md-6 col-xl-3 mt-3">
                                <label for="exampleFormControlTextarea1"><b> Image Slider</b></label>
                                <div class="input-group">
                                    <div class="input-group">
                                        <div class="form-group">
                                            <input type="file" class="form-control-file" id="img" name="img">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-3 mt-4">
                                <div class="input-group">
                                    <div class="input-group">
                                        <div class="form-group">
                                            <button type="submit" class="btn  btn-primary mb-2">Tambah</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Slider ID</th>
                                        <th>Title</th>
                                        <th>Caption</th>
                                        <th>Image</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($slider->data as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->title }}</td>
                                        <td>{{ $item->caption }}</td>
                                        <td>{{ $item->img }}</td>
                                        <td>
                                            <a href="{{url('/manage-landingpage/slider/delete/'.$item->id.'')}}" class="badge badge-danger"><i class="feather icon-trash-2"></i></a>
                                            <a href="{{url('/edit/slider/'.$item->id.'')}}" class="badge badge-primary"><i class="feather icon-edit"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div>
                            <nav aria-label="Page navigation example" style="float: right;">
                                <ul class="pagination">
                                    <li class="page-item"><a class="page-link" href="{{ $slider->prev_page_url }}">Previous</a></li>
                                    @php
                                        $counter = count($slider->links);
                                        $i = 0;
                                    @endphp
                                    @if ($counter > 0)
                                        @foreach ($slider->links as $item)
                                            @if($i > 2 && $i < $counter-1)
                                                <li class="page-item"><a class="page-link" href="{{ $item->url }}">{{ $i }}</a></li>
                                            @endif
                                        @php
                                           $i++; 
                                        @endphp
                                        @endforeach
                                    @endif
                                    <li class="page-item"><a class="page-link" href="{{ $slider->next_page_url }}">Next</a></li>
                                </ul>
                            </nav>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-header">
                <h4><b>Section 2</b></h4>
            </div>
            <div class="card-body p-0">
                <div class="container-fluid">
                    <form class="form-group" method="POST" action="{{ route('update-kontak-info') }}" enctype="multipart/form-data">
                        @csrf
                        @php
                            $counter = 1;
                        @endphp
                        @foreach ($kontak_info as $item)
                        <div class="card-header">
                            <h5><b>{{$item->title}}</b></h5>
                            <hr>
                            <div class="row mb-3">
                                <div class="form-group col-xl-6 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b> Title</b></label>
                                    <input type="text" class="form-control" id="title" name="title{{$counter}}" aria-describedby="title" placeholder="Masukan Title" value="{{$item->title}}">
                                    <small id="title" class="form-text text-muted">Ubah title sesuai kebutuhan</small>
                                    {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                                </div>
                                <div class="form-group col-xl-6 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b>Caption</b></label>
                                    <input type="text" class="form-control" id="caption" name="caption{{$counter}}" aria-describedby="title" placeholder="Masukan Caption" value="{{$item->caption}}">
                                    <small id="caption" class="form-text text-muted">Ubah caption sesuai kebutuhan</small>
                                    {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                                </div>
                            </div>
                            <input type="text" style="display: none;" name="id_kontak_info{{$counter}}" value="{{$item->id}}">
                        </div>
                        @php
                            $counter++;
                        @endphp
                        @endforeach
                        <div class="card-header">
                            <h5><b>Jam Buka</b></h5>
                            <hr>
                            @php
                            $counter = 1;
                            @endphp
                            @foreach ($jam_buka as $item)
                            <div class="row mb-3">
                                <div class="form-group col-xl-6 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b> Hari</b></label>
                                    <input type="text" class="form-control" id="title" name="hari{{$counter}}" aria-describedby="title" placeholder="Masukan Title" value="{{$item->hari}}">
                                    {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                                </div>
                                <div class="form-group col-xl-6 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b>Jam</b></label>
                                    <input type="text" class="form-control" id="caption" name="jam{{$counter}}" aria-describedby="title" placeholder="Masukan jam" value="{{$item->jam}}">
                                    {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                                </div>
                                <input type="text" style="display: none;" name="id_jam_buka{{$counter}}" value="{{$item->id}}">
                            </div>
                            @php
                            $counter++;
                            @endphp
                            @endforeach
                        </div>
                        <div class="card-header">
                            <div class="row mb-3">
                                <div class="form-group col-xl-4 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b> Title Content</b></label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" name="title_content" rows="3">{{$content_section_2->title}}</textarea>
                                    {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                                </div>
                                <div class="form-group col-xl-4 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b>Caption Content</b></label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" name="caption_content" rows="3">{{$content_section_2->caption}}</textarea>
                                    <small id="caption" class="form-text text-muted">Ubah caption sesuai kebutuhan</small>
                                    {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                                </div>
                                <div class="form-group col-xl-4 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b>Detail Caption Content</b></label>
                                    {{-- <input type="text" class="form-control" id="caption" name="detail_caption_content" aria-describedby="title" placeholder="Masukan Caption" value="{{$content_section_2->caption1}}"> --}}
                                    <textarea class="form-control" id="exampleFormControlTextarea1" name="detail_caption_content" rows="3">{{$content_section_2->caption1}}</textarea>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="form-group col-xl-4 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b>1. List Detail Content</b></label>
                                    <input type="text" class="form-control" id="title" name="list1" aria-describedby="title" placeholder="Masukan Title" value="{{$content_section_2->caption2}}">
                                    <small id="title" class="form-text text-muted">Ubah caption sesuai kebutuhan</small>
                                    {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                                </div>
                                <div class="form-group col-xl-4 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b>2. List Detail Content</b></label>
                                    <input type="text" class="form-control" id="caption" name="list2" aria-describedby="title" placeholder="Masukan Caption" value="{{$content_section_2->caption3}}">
                                    <small id="caption" class="form-text text-muted">Ubah caption sesuai kebutuhan</small>
                                    {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                                </div>
                                <div class="form-group col-xl-4 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b>3. List Detail Content</b></label>
                                    <input type="text" class="form-control" id="caption" name="list3" aria-describedby="title" placeholder="Masukan Caption" value="{{$content_section_2->caption4}}">
                                    <small id="caption" class="form-text text-muted">Ubah caption sesuai kebutuhan</small>
                                    {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-3 mt-4">
                            <div class="input-group">
                                <div class="input-group">
                                    <div class="form-group">
                                        <button type="submit" class="btn  btn-primary mb-2">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-header">
                <h4><b>Section 3</b></h4>
            </div>
            <div class="card-body p-0">
                <div class="container-fluid">
                    <form class="form-group" method="POST" action="{{ route('update-section-3') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-header">
                            <div class="row mb-3">
                                <div class="form-group col-xl-4 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b> Title Section</b></label>
                                    <input type="text" class="form-control" id="title" name="title_section" aria-describedby="title" placeholder="Masukan Title" value="{{$content_section_3->title}}">
                                    <small id="title" class="form-text text-muted">Ubah title sesuai kebutuhan</small>
                                    {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                                </div>
                            </div>
                            @php
                                use App\Models\CaptionDetail;
                                $detail = CaptionDetail::where('id_landing_page',$content_section_3->id)->get();
                                $counter = 1;
                                $card = 1;
                            @endphp
                            @foreach ($detail as $item)
                            @if ($item->id == 4)
                            <div class="row mb-3">
                                <div class="form-group col-xl-3 col-md-6  mt-3">
                                    <input type="text" style="display: none;" name="id{{$counter}}" value="{{$item->id}}">
                                    <label for="exampleFormControlTextarea1"><b> Title Card {{$card}}</b></label>
                                    <input type="text" class="form-control" id="title" name="title{{$counter}}" aria-describedby="title" placeholder="Masukan Title" value="{{$item->title_detail}}">
                                    <small id="title" class="form-text text-muted">Ubah title sesuai kebutuhan</small>
                                    @php
                                        $counter++;
                                    @endphp
                                    {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                                </div>
                                <div class="form-group col-xl-3 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b>List 1</b></label>
                                    <input type="text" class="form-control" id="caption" name="list{{$counter}}" aria-describedby="title" placeholder="Masukan Caption" value="{{$item->caption2}}">
                                    <small id="caption" class="form-text text-muted">Ubah caption sesuai kebutuhan</small>
                                    @php
                                        $counter++;
                                    @endphp
                                    {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                                </div>
                            </div>
                            @else  
                                <div class="row mb-3">
                                    <div class="form-group col-xl-3 col-md-6  mt-3">
                                        <input type="text" style="display: none;" name="id{{$counter}}" value="{{$item->id}}">
                                        <label for="exampleFormControlTextarea1"><b> Title Card {{$card}}</b></label>
                                        <input type="text" class="form-control" id="title" name="title{{$counter}}" aria-describedby="title" placeholder="Masukan Title" value="{{$item->title_detail}}">
                                        <small id="title" class="form-text text-muted">Ubah title sesuai kebutuhan</small>
                                        @php
                                            $counter++;
                                        @endphp
                                        {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                                    </div>
                                    <div class="form-group col-xl-3 col-md-6  mt-3">
                                        <label for="exampleFormControlTextarea1"><b>List 1</b></label>
                                        <input type="text" class="form-control" id="caption" name="list{{$counter}}" aria-describedby="title" placeholder="Masukan Caption" value="{{$item->caption2}}">
                                        <small id="caption" class="form-text text-muted">Ubah caption sesuai kebutuhan</small>
                                        @php
                                            $counter++;
                                        @endphp
                                        {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                                    </div>
                                    <div class="form-group col-xl-3 col-md-6  mt-3">
                                        <label for="exampleFormControlTextarea1"><b>List 2</b></label>
                                        <input type="text" class="form-control" id="caption" name="list{{$counter}}" aria-describedby="title" placeholder="Masukan Caption" value="{{$item->caption3}}">
                                        <small id="caption" class="form-text text-muted">Ubah caption sesuai kebutuhan</small>
                                        @php
                                            $counter++;
                                        @endphp
                                        {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                                    </div>
                                    <div class="form-group col-xl-3 col-md-6  mt-3">
                                        <label for="exampleFormControlTextarea1"><b>List 3</b></label>
                                        <input type="text" class="form-control" id="caption" name="list{{$counter}}" aria-describedby="title" placeholder="Masukan Caption" value="{{$item->caption4}}">
                                        <small id="caption" class="form-text text-muted">Ubah caption sesuai kebutuhan</small>
                                        @php
                                            $counter++;
                                        @endphp
                                        {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                                    </div>
                                </div>
                            @endif
                            @php
                            $card++;
                            @endphp
                            @endforeach
                           
                        <div class="col-md-6 col-xl-3 mt-4">
                            <div class="input-group">
                                <div class="input-group">
                                    <div class="form-group">
                                        <button type="submit" class="btn  btn-primary mb-2">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-header">
                <h4><b>Section 4</b></h4>
            </div>
            <div class="card-body p-0">
                <div class="container-fluid">
                    <form class="form-group" method="POST" action="{{ route('update-section-4') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-header">
                            <div class="row mb-3">
                                <div class="form-group col-xl-4 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b> Title Section</b></label>
                                    <input type="text" style="display: none;" name="id_main" value="{{$content_section_4->id}}">
                                    {{-- <input type="text" class="form-control" id="title" name="title_section" aria-describedby="title" placeholder="Masukan Title" value="{{$content_section_4->title}}"> --}}
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="title_section">{{$content_section_4->title}}</textarea>
                                    <small id="title" class="form-text text-muted">Ubah title sesuai kebutuhan</small>
                                    
                                </div>
                                <div class="form-group col-xl-4 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b> Caption Section</b></label>
                                    {{-- <input type="text" class="form-control" id="title" name="title_section" aria-describedby="title" placeholder="Masukan Title" value="{{$content_section_4->title}}"> --}}
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="caption_section">{{$content_section_4->caption}}</textarea>
                                    <small id="title" class="form-text text-muted">Ubah title sesuai kebutuhan</small>
                                    
                                </div>
                            </div>
                            @php
                                // use App\Models\CaptionDetail;
                                $detail = CaptionDetail::where('id_landing_page',$content_section_4->id)->get();
                                $counter = 1;
                                $card = 1;
                            @endphp
                            @foreach ($detail as $item)
                            <div class="row mb-3">
                                <div class="form-group col-xl-3 col-md-6  mt-3">
                                    <input type="text" style="display: none;" name="id{{$counter}}" value="{{$item->id}}">
                                    <label for="exampleFormControlTextarea1"><b> Title Card {{$card}}</b></label>
                                    <input type="text" class="form-control" id="title" name="title{{$counter}}" aria-describedby="title" placeholder="Masukan Title" value="{{$item->title_detail}}">
                                    <small id="title" class="form-text text-muted">Ubah title sesuai kebutuhan</small>
                                    {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                                </div>
                                <div class="col-md-6 col-xl-3 mt-3">
                                    <label for="exampleFormControlTextarea1"><b>Image</b></label>
                                    <div class="input-group">
                                        <div class="input-group">
                                            <div class="form-group">
                                                <input type="file" class="form-control-file" id="img" name="img{{$counter}}" value="{{$item->img}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @php
                                $counter++;
                            @endphp
                            </div>
                            @php
                            $card++;
                            @endphp
                            @endforeach
                           
                        <div class="col-md-6 col-xl-3 mt-4">
                            <div class="input-group">
                                <div class="input-group">
                                    <div class="form-group">
                                        <button type="submit" class="btn  btn-primary mb-2">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-header">
                <h3>Section 5</h3>
            </div>
            <div class="card-header">
                <h4><b>Founder</b></h4>
            </div>
            <div class="card-body p-0">
                <div class="container-fluid">
                    <form class="form-group" method="POST" action="{{ route('add-founder') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-header">
                            <div class="row mb-3">
                                <div class="form-group col-xl-3 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b>Nama</b></label>
                                    <input type="text" class="form-control" id="nama" name="nama" aria-describedby="title" placeholder="Masukan Nama">
                                    <small id="title" class="form-text text-muted">Ubah nama sesuai kebutuhan</small>
                                </div>
                                <div class="form-group col-xl-3 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b>Jabatan</b></label>
                                    <input type="text" class="form-control" id="jabatan" name="jabatan" aria-describedby="title" placeholder="Masukan jabatan">
                                    <small id="caption" class="form-text text-muted">Ubah jabatan sesuai kebutuhan</small>
                                </div>
                                <div class="form-group col-xl-3 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b>Caption</b></label>
                                    <input type="text" class="form-control" id="caption" name="caption" aria-describedby="title" placeholder="Masukan Caption">
                                    <small id="caption" class="form-text text-muted">Ubah caption sesuai kebutuhan</small>
                                </div>
                                <div class="col-md-6 col-xl-3 mt-3">
                                    <label for="exampleFormControlTextarea1"><b>Foto</b></label>
                                    <div class="input-group">
                                        <div class="input-group">
                                            <div class="form-group">
                                                <input type="file" class="form-control-file" id="foto" name="img">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-xl-3 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b>Link Facebook</b></label>
                                    <input type="text" class="form-control" id="link_facebook" name="link_facebook" aria-describedby="link_facebook" placeholder="Masukan link_facebook">
                                    <small id="link_facebook" class="form-text text-muted">Ubah link facebook sesuai kebutuhan</small>
                                </div>
                                <div class="form-group col-xl-3 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b>Link Instagram</b></label>
                                    <input type="text" class="form-control" id="link_instagram" name="link_instagram" aria-describedby="title" placeholder="Masukan link_instagram">
                                    <small id="link_instagram" class="form-text text-muted">Ubah link instagram sesuai kebutuhan</small>
                                </div>
                                <div class="form-group col-xl-3 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b>Link Twitter</b></label>
                                    <input type="text" class="form-control" id="link_twitter" name="link_twitter" aria-describedby="title" placeholder="Masukan link_twitter">
                                    <small id="link_twitter" class="form-text text-muted">Ubah link_twitter sesuai kebutuhan</small>
                                </div>
                                <div class="col-md-6 col-xl-3 mt-4">
                                    <div class="input-group">
                                        <div class="input-group">
                                            <div class="form-group">
                                                <button type="submit" class="btn  btn-primary mb-2">Tambah</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Founder ID</th>
                                            <th>Nama</th>
                                            <th>Jabatan</th>
                                            <th>Foto</th>
                                            <th>Caption</th>
                                            <th>Link Facebook</th>
                                            <th>Link Instagram</th>
                                            <th>Link Twitter</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($founder->data as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->nama }}</td>
                                            <td>{{ $item->jabatan }}</td>
                                            <td>{{ $item->img }}</td>
                                            <td>{{ $item->caption }}</td>
                                            <td>{{ $item->link_facebook }}</td>
                                            <td>{{ $item->link_instagram }}</td>
                                            <td>{{ $item->link_twitter }}</td>
                                            <td>
                                                <a href="{{url('/manage-landingpage/founder/delete/'.$item->id.'')}}" class="badge badge-danger"><i class="feather icon-trash-2"></i></a>
                                                <a href="{{url('/edit/founder/'.$item->id.'')}}" class="badge badge-primary"><i class="feather icon-edit"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div>
                            <nav aria-label="Page navigation example" style="float: right;">
                                <ul class="pagination">
                                    <li class="page-item"><a class="page-link" href="{{ $founder->prev_page_url }}">Previous</a></li>
                                    @php
                                        $counter = count($founder->links);
                                        $i = 0;
                                    @endphp
                                    @if ($counter > 0)
                                        @foreach ($founder->links as $item)
                                            @if($i > 2 && $i < $counter-1)
                                                <li class="page-item"><a class="page-link" href="{{ $item->url }}">{{ $i }}</a></li>
                                            @endif
                                        @php
                                           $i++; 
                                        @endphp
                                        @endforeach
                                    @endif
                                    <li class="page-item"><a class="page-link" href="{{ $founder->next_page_url }}">Next</a></li>
                                </ul>
                            </nav>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-header">
                <h4><b>Section 6</b></h4>
            </div>
            <div class="card-body p-0">
                <div class="container-fluid">
                    <form class="form-group" method="POST" action="{{ route('update-section-6') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-header">
                            <div class="row mb-3">
                                <div class="form-group col-xl-4 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b> Title Section</b></label>
                                    <input type="text" style="display: none;" name="id_main" value="{{$content_section_6->id}}">
                                    {{-- <input type="text" class="form-control" id="title" name="title_section" aria-describedby="title" placeholder="Masukan Title" value="{{$content_section_4->title}}"> --}}
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="title_section">{{$content_section_6->title}}</textarea>
                                    <small id="title" class="form-text text-muted">Ubah title sesuai kebutuhan</small>
                                    
                                </div>
                                <div class="form-group col-xl-4 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b> Caption Section</b></label>
                                    {{-- <input type="text" class="form-control" id="title" name="title_section" aria-describedby="title" placeholder="Masukan Title" value="{{$content_section_4->title}}"> --}}
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="caption_section">{{$content_section_6->caption}}</textarea>
                                    <small id="title" class="form-text text-muted">Ubah title sesuai kebutuhan</small>
                                    
                                </div>
                            </div>
                            @php
                                // use App\Models\CaptionDetail;
                                $detail = CaptionDetail::where('id_landing_page',$content_section_6->id)->get();
                                $counter = 1;
                                $card = 1;
                            @endphp
                            @foreach ($detail as $item)
                            <div class="row mb-3">
                                <div class="form-group col-xl-3 col-md-6  mt-3">
                                    <input type="text" style="display: none;" name="id{{$counter}}" value="{{$item->id}}">
                                    <label for="exampleFormControlTextarea1"><b> Title Card {{$card}}</b></label>
                                    {{-- <input type="text" class="form-control" id="title" name="title{{$counter}}" aria-describedby="title" placeholder="Masukan Title" value="{{$item->title_detail}}"> --}}
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="title{{$counter}}">{{$item->title_detail}}</textarea>
                                    <small id="title" class="form-text text-muted">Ubah title sesuai kebutuhan</small>
                                    {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                                </div>
                                <div class="form-group col-xl-3 col-md-6  mt-3">
                                    <input type="text" style="display: none;" name="id{{$counter}}" value="{{$item->id}}">
                                    <label for="exampleFormControlTextarea1"><b> Detail Caption {{$card}}</b></label>
                                    {{-- <input type="text" class="form-control" id="title" name="title{{$counter}}" aria-describedby="title" placeholder="Masukan Title" value="{{$item->caption1}}"> --}}
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="caption{{$counter}}">{{$item->caption1}}</textarea>
                                    <small id="title" class="form-text text-muted">Ubah title sesuai kebutuhan</small>
                                    {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                                </div>
                                @php
                                $counter++;
                            @endphp
                            </div>
                            @php
                            $card++;
                            @endphp
                            @endforeach
                           
                        <div class="col-md-6 col-xl-3 mt-4">
                            <div class="input-group">
                                <div class="input-group">
                                    <div class="form-group">
                                        <button type="submit" class="btn  btn-primary mb-2">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-header">
                <h4><b>Section 7</b></h4>
            </div>
            <div class="card-body p-0">
                <div class="container-fluid">
                    <form class="form-group" method="POST" action="{{ route('update-section-7') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-header">
                            <div class="row mb-3">
                                <div class="form-group col-xl-4 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b> Title Section</b></label>
                                    <input type="text" style="display: none;" name="id_main" value="{{$content_section_7->id}}">
                                    {{-- <input type="text" class="form-control" id="title" name="title_section" aria-describedby="title" placeholder="Masukan Title" value="{{$content_section_4->title}}"> --}}
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="title_section">{{$content_section_7->title}}</textarea>
                                    <small id="title" class="form-text text-muted">Ubah title sesuai kebutuhan</small>
                                    
                                </div>
                                <div class="form-group col-xl-4 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b> Caption Section</b></label>
                                    {{-- <input type="text" class="form-control" id="title" name="title_section" aria-describedby="title" placeholder="Masukan Title" value="{{$content_section_4->title}}"> --}}
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="caption_section">{{$content_section_7->caption}}</textarea>
                                    <small id="title" class="form-text text-muted">Ubah title sesuai kebutuhan</small>
                                    
                                </div>
                            </div>
                        <div class="col-md-6 col-xl-3 mt-4">
                            <div class="input-group">
                                <div class="input-group">
                                    <div class="form-group">
                                        <button type="submit" class="btn  btn-primary mb-2">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-header">
                <h4><b>Section 11</b></h4>
            </div>
            <div class="card-body p-0">
                <div class="container-fluid">
                    <form class="form-group" method="POST" action="{{ route('update-section-11') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-header">
                            <div class="row mb-3">
                                <div class="form-group col-xl-4 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b> Title Section</b></label>
                                    <input type="text" style="display: none;" name="id_main" value="{{$content_section_11->id}}">
                                    {{-- <input type="text" class="form-control" id="title" name="title_section" aria-describedby="title" placeholder="Masukan Title" value="{{$content_section_4->title}}"> --}}
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="title_section">{{$content_section_11->title}}</textarea>
                                    <small id="title" class="form-text text-muted">Ubah title sesuai kebutuhan</small>
                                    
                                </div>
                                <div class="form-group col-xl-4 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b> Caption Section</b></label>
                                    {{-- <input type="text" class="form-control" id="title" name="title_section" aria-describedby="title" placeholder="Masukan Title" value="{{$content_section_4->title}}"> --}}
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="caption_section">{{$content_section_11->caption}}</textarea>
                                    <small id="title" class="form-text text-muted">Ubah title sesuai kebutuhan</small>
                                    
                                </div>
                                <div class="form-group col-xl-4 col-md-6  mt-3">
                                    <label for="exampleFormControlTextarea1"><b> Link Button</b></label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="no_telp">{{$content_section_11->no_telp}}</textarea>
                                    <small id="title" class="form-text text-muted">Ubah link sesuai kebutuhan</small>
                                    
                                </div>
                            </div>
                        <div class="col-md-6 col-xl-3 mt-4">
                            <div class="input-group">
                                <div class="input-group">
                                    <div class="form-group">
                                        <button type="submit" class="btn  btn-primary mb-2">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection