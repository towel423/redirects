@extends('layouts.dashboard')
@section('dashboard-favicon')
    <link rel="icon" href="{{asset('assets/images/favicon/'.$web_setting->favicon.'')}}" type="image/x-icon">
@endsection
@section('bearcrumb')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5 class="m-b-10">Header Footer Setting</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#!">Header Footer Setting</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
@section('custom-script')
{{-- @if (isset($message)) --}}

@if($errors->any())
    <script>
        window.addEventListener('load', function() {
        $("#success").show().delay(5000).fadeOut();
    })
    </script>
@else
    <script>
        window.addEventListener('load', function() {
            $("#success").hide();
        })
    </script>
@endif
@endsection
@section('content')
<div class="alert alert-info" role="alert" id="success">
    Data Berhasil Disimpan
</div>
<div class="row">
    <!-- prject ,team member start -->
    <div class="col-xl-12 col-md-12">
        <div class="card table-card">
            <div class="card-header">
                <h3>Header Footer Setting</h3>
            </div>
            <div class="card-body p-0">
                <div class="container-fluid">
                    <form class="form-group" method="POST" action="{{route('header-footer')}}">
                        @csrf
                        <div class="row mb-3">
                            <div class="form-group col-xl-10 col-md-10  mt-3">
                                <label for="exampleFormControlTextarea1"><b>Kontak Nomor Telepon/ Whatsapp</b></label>
                                <input type="text" class="form-control" id="no_telp" name="no_telp" aria-describedby="no_telp" placeholder="Masukan Nomor Kontak" value="{{$header_setting->no_telp}}">
                                <small id="no_telp" class="form-text text-muted">Nomor telepon</small>
                                {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="form-group col-xl-10 col-md-10  mt-3">
                                <label for="exampleFormControlTextarea1"><b>Caption Kontak</b></label>
                                <input type="text" class="form-control" id="caption_kontak" name="caption" aria-describedby="caption_kontak" placeholder="Masukan Caption" value="{{$header_setting->caption}}">
                                <small id="caption_kontak" class="form-text text-muted">Caption Kontak</small>
                                {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="form-group col-xl-10 col-md-10  mt-3">
                                <label for="exampleFormControlTextarea1"><b>Link Instagram</b></label>
                                <input type="text" class="form-control" id="link_instagram" name="link_instagram" aria-describedby="link_instagram" placeholder="Masukan Link Instagram" value="{{$header_setting->link_instagram}}">
                                <small id="link_instagram" class="form-text text-muted">Link Instagram</small>
                                {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="form-group col-xl-10 col-md-10  mt-3">
                                <label for="exampleFormControlTextarea1"><b>Link Facebook</b></label>
                                <input type="text" class="form-control" id="link_facebook" name="link_facebook" aria-describedby="link_facebook" placeholder="Masukan Link Facebook" value="{{$header_setting->link_facebook}}">
                                <small id="link_facebook" class="form-text text-muted">Link Facebook</small>
                                {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="form-group col-xl-10 col-md-10  mt-3">
                                <label for="exampleFormControlTextarea1"><b>Link Twitter</b></label>
                                <input type="text" class="form-control" id="link_twitter" name="link_twitter" aria-describedby="link_twitter" placeholder="Masukan Link Twitter" value="{{$header_setting->link_twitter}}">
                                <small id="link_twitter" class="form-text text-muted">Link Twitter</small>
                                {{-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> --}}
                            </div>
                        </div>
                        {{-- <div class="row">
                            <div class="col-md-3 col-xl-3 mb-5">
                                <label for="exampleFormControlTextarea1"><b>Favicon Web</b></label>
                                <div class="card mb-3">
                                    <img class="img-thumbnail" style="" id="preview-background" src="{{asset('assets/images/favicon/'..'')}}">
                                </div>
                                <div class="input-group">
                                    <div class="input-group">
                                        <div class="form-group">
                                            <input type="file" class="form-control-file" id="background" name="background">
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                            {{-- <div class="col-md-3 col-xl-3 mb-5">
                                <label for="exampleFormControlTextarea1"><b>Logo Web</b></label>
                                <div class="card mb-3">
                                    <img class="img-thumbnail" id="preview-slider" src="{{asset('assets/images/logo/'..'')}}">
                                </div>
                                <div class="input-group">
                                    <div class="form-group">
                                        <input type="file" class="form-control-file" id="slider" name="slider_1">
                                    </div>
                                </div>
                            </div> --}}
                            {{-- 
                            <div class="col-md-3 col-xl-3 mb-5">
                                <label for="exampleFormControlTextarea1"><b> Image Slider 2</b></label>
                                <div class="card mb-3">
                                    <img class="img-thumbnail" id="preview-slider-2" src="{{asset('dist/assets/images/slider/img-slide-3.jpg')}}">
                                </div>
                                <div class="input-group">
                                    <div class="form-group">
                                        <input type="file" class="form-control-file" id="slider_2" name="slider_2">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-xl-3 mb-5">
                                <label for="exampleFormControlTextarea1"><b> Image Slider 3</b></label>
                                <div class="card mb-3">
                                    <img class="img-thumbnail" id="preview-slider-3" src="{{asset('dist/assets/images/slider/img-slide-3.jpg')}}">
                                </div>
                                <div class="input-group">
                                    <div class="form-group">
                                        <input type="file" class="form-control-file" id="slider_3" name="slider_3">
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                        <div class="form-group col-xl-2 col-md-2">
                            <button type="submit" class="btn  btn-primary mb-2">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection