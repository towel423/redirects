<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Instagram extends Model
{
    use HasFactory;
    protected $table = 'data_instagram';
    protected $fillable = [
        'media_id',
        'username',
        'caption',
        'media_type',
        'media_url',
        'permalink',
        'created_at',
        'updated_at',
    ];

    public function refreshToken($token)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://graph.instagram.com/refresh_access_token?access_token='.$token.'&grant_type=ig_refresh_token',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Cookie: csrftoken=q9FexLEwlWVHeurLowl3qCFmR7AxF0G3; ig_did=02CB5DD7-5A74-4EEA-82FB-71D2AC0178D5; ig_nrcb=1; mid=YX9JWgAEAAGI9Ehr0_1aTMok1Np6'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
    
    public function curl($token)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://graph.instagram.com/v12.0/me?fields=id,username,media,media_count,account_type&access_token='.$token.'',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Cookie: csrftoken=q9FexLEwlWVHeurLowl3qCFmR7AxF0G3; ig_did=02CB5DD7-5A74-4EEA-82FB-71D2AC0178D5; ig_nrcb=1; mid=YX9JWgAEAAGI9Ehr0_1aTMok1Np6'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    public function get_media($media_id,$token)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://graph.instagram.com/'.$media_id.'?fields=id,username,caption,media_type,media_url,permalink,thumbnail_url,timestamp&access_token='.$token.'',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Cookie: csrftoken=q9FexLEwlWVHeurLowl3qCFmR7AxF0G3; ig_did=02CB5DD7-5A74-4EEA-82FB-71D2AC0178D5; ig_nrcb=1; mid=YX9JWgAEAAGI9Ehr0_1aTMok1Np6'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
}
