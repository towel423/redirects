<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Instagram;
use App\Models\Reservasi;
use App\Models\LandingPage;
use App\Models\WebSetting;
use App\Models\CaptionDetail;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;


class RedirectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function redirect_home()
    {
        $url = "https://kampoonghening.com";
        return redirect()->away($url);
    }

    public function redirect_param(Request $request)
    {
        $basic = "https://konten.kampoonghening.com/";
        $param = $request->route('param');
        $url = $basic.$param;
        return redirect()->away($url);
    }
}
